﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using PMS.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using PMS.Utility;
using Microsoft.AspNetCore.Http;

namespace PMS.Services
{
    [AllowAnonymous]

    public class Notifications
    {

        private readonly ApplicationDbContext _db;
        private readonly IConfiguration _config;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly IHttpContextAccessor _httpContextAccessor;


        int studentID = 0;
        public Notifications(UserManager<ApplicationUser> userManager,
        SignInManager<ApplicationUser> signInManager, ApplicationDbContext _db, IConfiguration config, IHttpContextAccessor httpContextAccessor)
        {
            this._db = _db;
            _config = config;
            this.userManager = userManager;
            this.signInManager = signInManager;
            _httpContextAccessor = httpContextAccessor;


        }

        public List<Tender> GetCustomiseNotifications()
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            List<Tender> CustomiseNotifications = new List<Tender>();

            var userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);
            Console.WriteLine("Hello");
            try
            {
                conn.Open();

                cmdB = new SqlCommand();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Select_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Select_Notifications");
                cmdB.Parameters.AddWithValue("@varID", userId);

                cmdB.Connection = conn;
                SqlDataReader drB = cmdB.ExecuteReader();



                while (drB.Read())
                {
                    CustomiseNotifications.Add(new Tender
                    {
                        Id = int.Parse(drB["NTID"].ToString()),
                        Notification = drB["Message"].ToString(),
                        Link = drB["Link"].ToString(),
                        CreatedDate = drB["CreatedDate"].ToString()

                    });

                }
                cmdB.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                conn.Close();
            };
            Console.WriteLine(CustomiseNotifications.Count);

            return CustomiseNotifications;
        }


        public List<Tender> GetTenderNotifications()
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            List<Tender> CourseNotifications = new List<Tender>();

            var userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);

            Console.WriteLine(userId);
            try
            {
                conn.Open();
                cmdB = new SqlCommand();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_NE_Notification_Student";
                cmdB.Parameters.AddWithValue("@varStepName", "Next_Course_Notification");
                cmdB.Parameters.AddWithValue("@varUserID", userId);

                cmdB.Connection = conn;
                SqlDataReader drB = cmdB.ExecuteReader();



                while (drB.Read())
                {
                    CourseNotifications.Add(new Tender
                    {
                        Notification = "Buy Next Course - "+drB["NextCourseName"].ToString(),
                        //CourseID = int.Parse(drB["NextCourseID"].ToString())

                    });

                }
                cmdB.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                conn.Close();
            };
            return CourseNotifications;
        }



        //public List<CustomiseNotifications> GetStudentCourseNotifications()
        //{

        //    SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
        //    SqlCommand cmdB = new SqlCommand();
        //    List<CustomiseNotifications> GetStudentCourseNotifications = new List<CustomiseNotifications>();

        //    var userId = _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier);

        //    Console.WriteLine("Hi");
        //    try
        //    {
        //        conn.Open();
        //        cmdB = new SqlCommand();
        //        cmdB.CommandType = CommandType.StoredProcedure;
        //        cmdB.CommandText = "PROC_NE_Notification_Student";
        //        cmdB.Parameters.AddWithValue("@varStepName", "Select_Course_Notification");
        //        cmdB.Parameters.AddWithValue("@varUserID", userId);

        //        cmdB.Connection = conn;
        //        SqlDataReader drB = cmdB.ExecuteReader();



        //        while (drB.Read())
        //        {
        //            GetStudentCourseNotifications.Add(new CustomiseNotifications
        //            {
        //                Notification = drB["Notification"].ToString(),

        //            });

        //        }
        //        cmdB.Dispose();
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //    }
        //    finally
        //    {
        //        conn.Close();
        //    };
        //    return GetStudentCourseNotifications;
        //}







    }
}
