﻿using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.Extensions.Options;
using PMS.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PMS.Services
{
    public class EmailSender : IEmailSender
    {
        public EmailOptions Options { get; set; }

        public EmailSender(IOptions<EmailOptions> emailOptions)
        {
            Options = emailOptions.Value;
        }

        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            throw new NotImplementedException();
        }

        //    public Task SendEmailAsync(string email, string subject, string message)
        //    {
        //        return Execute(Options.SendGridKey, subject, message, email);
        //    }

        //    private Task Execute(string sendGridKey, string subject, string message, string email)
        //    {
        //        var client = new SendGridClient(sendGridKey);
        //        var msg = new SendGridMessage()
        //        {
        //            From = new EmailAddress("niraeducation2019@gmail.com", "NIRA"),
        //            Subject = subject,
        //            PlainTextContent = message,
        //            HtmlContent = message
        //        };
        //        msg.AddTo(new EmailAddress(email));
        //        try
        //        {
        //            return client.SendEmailAsync(msg);
        //        }
        //        catch (Exception ex)
        //        {

        //        }
        //        return null;
        //    }
    }
}
