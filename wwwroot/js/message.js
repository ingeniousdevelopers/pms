﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

        function PrintTable() {
            var printWindow = window.open('', '', 'height=200,width=400');
            printWindow.document.write('<html><head><title>Table Contents</title>');


            printWindow.document.write('</head>');

            printWindow.document.write('<body>');
            var divContents = document.getElementById("TenderWonT").innerHTML;
            printWindow.document.write(divContents);
            printWindow.document.write('</body>');

            printWindow.document.write('</html>');
            printWindow.document.close();
            printWindow.print();
        }

function msgSend() {
    if ((!$("#input_msg_send_chatapp").val().length == 0)) {
        $('<li class="self mb-10"><div class="self-msg-wrap"><div class="msg block pull-right">' + $("#input_msg_send_chatapp").val() + '<div class="msg-per-detail mt-5"><span class="msg-time txt-grey">3:30 pm</span></div></div></div><div class="clearfix"></div></li>').insertAfter(".chat-content  ul li:last-child");
    } else {
        alert('Please type asomthing!');
    }


    $.ajax({
        url: "/Admin/SendMessage",
        data: {
            message: $("#input_msg_send_chatapp").val(),
            TenderID: localStorage.getItem("TenderID")
        },
        success: function (result) {
            $(".chatdiv").empty();
            console.log(result.communication.length);
            for (var j = 0; j < result.communication.length; j++) {
                if (result.communication[j].type == "Self") {
                    $(".chatdiv").append($('<li class="self mb-10 pull-right" style="width:75%;"><div class="self-msg-wrap"><div class="msg block pull-right"><p><strong>' + result.communication[j].name + '</strong></p><p>' + result.communication[j].note + '</p><div class="msg-per-detail mt-5"><span class="msg-time">' + result.communication[j].createdDate + '</span></div></div></div><div class="clearfix"></div></li>'));


                } else {
                    $(".chatdiv").append($('<li class="friend pull-left" style="width:75%;"><div class="friend-msg-wrap"><div class="msg  pull-left"><p><strong>' + result.communication[j].name + '</strong></p><p>' + result.communication[j].note + '</p><div class="msg-per-detail mt-5"><span class="msg-time">' + result.communication[j].createdDate + '</span></div></div></div><div class="clearfix"></div></li>').insertAfter(".chat-content"));

                }

            }

  $(".chatdiv").stop().animate({
    scrollTop: $(".chatdiv li").last().offset().top
  }, '500', 'swing', function() {});
        }
    })
    $("#input_msg_send_chatapp").val("");
};


$(document).ready(function () {
    $(document).bind('keypress', function (e) {

        if (e.keyCode == 13 && $("#input_msg_send_chatapp").val() != "") {
            $.ajax({
                url: "/Admin/SendMessage",
                data: {
                    message: $("#input_msg_send_chatapp").val(),
                    TenderID: localStorage.getItem("TenderID")
                },
                success: function (result) {
                    $(".chatdiv").empty();
                    console.log(result.communication.length);
                    for (var j = 0; j < result.communication.length; j++) {
                        if (result.communication[j].type == "Self") {
                            $(".chatdiv").append($('<li class="self mb-10 pull-right" style="width:75%;"><div class="self-msg-wrap"><div class="msg block pull-right"><p><strong>' + result.communication[j].name + '</strong></p><p>' + result.communication[j].note + '</p><div class="msg-per-detail mt-5"><span class="msg-time">' + result.communication[j].createdDate + '</span></div></div></div><div class="clearfix"></div></li>'));


                        } else {
                            $(".chatdiv").append($('<li class="friend pull-left" style="width:75%;"><div class="friend-msg-wrap"><div class="msg  pull-left"><p><strong>' + result.communication[j].name + '</strong></p><p>' + result.communication[j].note + '</p><div class="msg-per-detail mt-5"><span class="msg-time">' + result.communication[j].createdDate + '</span></div></div></div><div class="clearfix"></div></li>').insertAfter(".chat-content"));

                        }

                    }
                    $("#input_msg_send_chatapp").val("");
                    $(".chatdiv").stop().animate({
                        scrollTop: $(".chatdiv li").last().offset().top
                    }, '500', 'swing', function () { });
                }
            })
        }
    });

    setInterval(function () {

     

            $.ajax({
                url: "/Admin/SendMessage",
                data: {
                    message: "",
                    TenderID: localStorage.getItem("TenderID")
                },
                success: function (result) {
                    $(".chatdiv").empty();
                    console.log(result.communication.length);
                    for (var j = 0; j < result.communication.length; j++) {
                        if (result.communication[j].type == "Self") {
                            $(".chatdiv").append($('<li class="self mb-10 pull-right" style="width:75%;"><div class="self-msg-wrap"><div class="msg block pull-right"><p><strong>' + result.communication[j].name + '</strong></p><p>' + result.communication[j].note + '</p><div class="msg-per-detail mt-5"><span class="msg-time">' + result.communication[j].createdDate + '</span></div></div></div><div class="clearfix"></div></li>'));


                        } else {
                            $(".chatdiv").append($('<li class="friend pull-left" style="width:75%;"><div class="friend-msg-wrap"><div class="msg  pull-left"><p><strong>' + result.communication[j].name + '</strong></p><p>' + result.communication[j].note + '</p><div class="msg-per-detail mt-5"><span class="msg-time">' + result.communication[j].createdDate + '</span></div></div></div><div class="clearfix"></div></li>').insertAfter(".chat-content"));

                        }

                    }
                    

                    //$(".chatdiv").stop().animate({
                    //    scrollTop: $(".chatdiv li").last().offset().top
                    //}, '500', 'swing', function () { });
                }
            })
            //$("#input_msg_send_chatapp").val("");
        
    }, 5000);
});

