﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PMS.Models
{
    public class Unit
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public String Name { get; set; }
    }
}
