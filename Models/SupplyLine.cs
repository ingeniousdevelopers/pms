﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PMS.Models
{
    public class SupplyLine
    {
        [Required]
        public int ID { get; set; }

        [Required]
        public String Name { get; set; }

        public String Description { get; set; }

        public String HSN { get; set; }

        public Double FI { get; set; }
        public Double GST { get; set; }

        public Double CESS { get; set; }

        public Double EWR { get; set; }

        public Double EWR_GST { get; set; }

        public Double EWR_FI { get; set; }

        public Double EWR_FI_GST { get; set; }


        public Double UnitRate { get; set; }

        public Double Amount { get; set; }


        public int DescriptionID { get; set; }
        public int UnitID { get; set; }
        public Double Quantity { get; set; }
        public int SupplyID { get; internal set; }
        public string Unit { get; internal set; }
        public double AvaQty { get; internal set; }
        public double PassedQty { get; internal set; }
    }
}
