﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PMS.Models
{
    public class ApplicationUser : IdentityUser<int>
    {
        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        [DataType(DataType.Text)]
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [Range(0,1)]
        public int IsAccountActive { get; set; }

        public String Picture { get; set; }

        public DateTime CreatedOn { get; set; }

        public String Address1 { get; set; }

        public String Address2 { get; set; }

        public String Address3 { get; set; }

        public String Department { get; set; }

        public String Type { get; set; }

        public int TenderID { get; set; }

        //public string Name { get;  set; }
        //public string RoleName { get;  set; }
    }
}
