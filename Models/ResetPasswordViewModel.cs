﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PMS.Models
{
    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = "Please enter valid Email ID")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please enter valid Password with minimum 6 characters")]
        [StringLength(30, MinimumLength = 6, ErrorMessage = "Please enter valid Password with minimum 6 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password",
            ErrorMessage = "Password and Confirm Password must match")]
        public string ConfirmPassword { get; set; }

        public string Token { get; set; }

        [Required(ErrorMessage = "Please enter valid OTP")]
        public string verifyCode { get; set; }

    }
}
