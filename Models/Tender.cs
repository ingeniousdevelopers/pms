﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PMS.Models
{
    public class Tender
    {

        public int Id { get; set; }

        public int ManagerID { get; set; }

        public int ResponsiblePerson { get; set; }



        public string TenderNo { get; set; }
        public string ManagerName { get; set; }

        public string Permission { get; set; }
        public string ModuleName { get; set; }



        public int TenderID { get; set; }

        public string ClientName { get; set; }

        public string Remark { get; set; }


        public string TenderName { get; set; }

        public string CriteriaCategory { get; set; }

        public string Category { get; set; }

        public string Industry { get; set; }

        public string BiddingType { get; set; }



        public string Criteria { get; set; }


        public Double WeightageMarks { get; set; }

        public Double VCSScore { get; set; }

        public string ScopeOfWork { get; set; }

        public string VCSSatisfied { get; set; }



        public string Region { get; set; }

        public string Source { get; set; }

        public string SourceDetails { get; set; }
        public string TenderNFDate { get; set; }
        public string BillSubOffline { get; set; }
        public string BillSubOnline { get; set; }

        public string PreBidDate { get; set; }

        public string Priority { get; set; }



        public string Status { get; set; }

        public string Designation { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string EMD { get; set; }

        public string EMDE { get; set; }

        public string TEx { get; set; }



        public string BECStatus { get; set; }

        public string SynopsisStatus { get; set; }

        public string ContactDetails { get; set; }

        public string TenderFeeDetails { get; set; }

        public string TenderType { get; set; }
         
        public string PreBidTime { get; set; }

        public string BidOpeningDate { get; set; }

        public string BidSDateOnline { get; set; }

        public string BidSDateOffline { get; set; }

        public string BidSTimeOnline { get; set; }
        public string BidSTimeOffline { get; set; }


        public string BidOpeningTime { get; set; }

        public string ContractValue { get; set; }

        public string ContractPeriod { get; set; }

        public string ExContractPeriod { get; set; }



        public string Name { get; set; }

        public string Content { get; set; }

        public string Defination { get; set; }

        public string Mode { get; set; }

        public string JV { get; set; }

        public string BidAward { get; set; }


        public Double TenderFee { get; set; }

        public string IsActive { get; set; }
        public string CreatedDate { get; set; }
        public string CreatedByUser { get;  set; }
        public string BidValidity { get; internal set; }
        public string ManpowerDetails { get; internal set; }
        public string AnyOther { get; internal set; }
        public string Competitors { get; internal set; }
        public string ParentCompanyExperience { get; internal set; }
        public string StartUpClause { get; internal set; }
        public string PurchasePreference { get; internal set; }
        public string PriceEscalation { get; internal set; }
        public string Insurance { get; internal set; }
        public string LiquidityDamage { get; internal set; }
        public string PaymentTerms { get; internal set; }
        public string Penalities { get; internal set; }
        public string DefaultPeriod { get; internal set; }
        public string SecurityDeposit { get; internal set; }
        public string MobAdvance { get; internal set; }
        public string MobPeriod { get; internal set; }
        public string GuestHouse { get; internal set; }
        public string OfficeSetup { get; internal set; }
        public string TransportationRequirement { get; internal set; }
        public string EquipmentConsumables { get; internal set; }
        public string ManpowerQualification { get; internal set; }
        public string Strength { get; internal set; }
        public string Weakness { get; internal set; }
        public string Oppurtunities { get; internal set; }
        public string Threats { get; internal set; }

        public string Stage { get; set; }

        public string TenderDocuments { get; set; }

        public string CompletedByDate { get; set; }

        public int PersonID { get; set; }

        public string Approval { get; set; }
        public string FirstName { get; internal set; }
        public string LastName { get; internal set; }
        public string Department { get; internal set; }
        public string Type { get; internal set; }
        public string BidSubmittedOn { get; internal set; }
        public string CompetitorName { get; internal set; }
        public string TenderRef { get; internal set; }
        public string HighLevScope { get; internal set; }
        public string Sector { get; internal set; }
        public string CostingGroup { get; internal set; }
        public string CoverageORDeployment { get; internal set; }
        public string CostEst { get; internal set; }
        public string EstimatedValue { get; internal set; }
        public string PriorityName { get; internal set; }
        public string CategoryName { get; internal set; }
        public string BiddingTypeName { get; internal set; }
        public string IndustryName { get; internal set; }
        public string TenderTypeName { get; internal set; }
        public string FileDetails { get; internal set; }
        public string Synopsistatus { get; internal set; }
        public string SrNo { get; internal set; }
        public string ArticleNo { get; internal set; }
        public string Article { get; internal set; }
        public string VCSQuery { get; internal set; }
        public string TenderCondition { get; internal set; }
        public string CustomerResponse { get; internal set; }
        public string CustomerResponseDoc { get; internal set; }
        public string ClarifiedDate { get; internal set; }
        public string RaisedBy { get; internal set; }
        public string ProjectCode { get; internal set; }
        public string LOANo { get; internal set; }
        public string ProjectName { get; internal set; }
        public string Client { get; internal set; }
        public string TenderCategory { get; internal set; }
        public string TenderIndustry { get; internal set; }
        public string Note { get; internal set; }
        public string IssueDate { get; internal set; }
        public string StartDate { get; internal set; }
        public string CompletionDate { get; internal set; }
        public string Winner { get; internal set; }
        public string WinnerQuotedValue { get; internal set; }
        public string OurQuotedValue { get; internal set; }
        public string LostBy { get; internal set; }
        public string ReasonForLoss { get; internal set; }
        public string QuotedPrice { get; internal set; }
        public string RemarksByP { get; internal set; }
        public string RemarksByM { get; internal set; }
        public string MonthName { get; internal set; }
        public string Year { get; internal set; }
        public string BECRemark { get; internal set; }
        public string Notification { get; internal set; }
        public string Link { get; internal set; }
    }
}
