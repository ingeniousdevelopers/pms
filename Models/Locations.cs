﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PMS.Models
{
    public class Locations
    {
        [Required]
        public int ID { get; set; }

        public int LocID { get; set; }

        public int DestID { get; set; }


        [Required]
        public String Name { get; set; }

        public String Information { get; set; }

        public String Photo { get; set; }

        public String Video { get; set; }


        public String SectorName { get; set; }

        public String CountryName { get; set; }

        public String StateName { get; set; }

        public String CategoryName { get; set; }

        public Double MinStay { get; set; }

        public String TourType { get; set; }

        public String LocationInfo { get; set; }

        public String Type { get; set; }

        public String Date { get; set; }

        public Double RoadD { get; set; }

        public Double JourneyHR { get; set; }
        public string DestinationName { get; internal set; }
    }
}
