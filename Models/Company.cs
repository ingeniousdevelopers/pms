﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PMS.Models
{
    public class Company
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Enter Company Name")]
        public string CompanyName { get; set; }


        [Required(ErrorMessage = "Enter Address")]
        public string Address { get; set; }

        [Required(ErrorMessage = "Enter Valid Phone No")]
        [RegularExpression(@"^(\d{10})$", ErrorMessage = "Mobile No is not valid")]
        public string PhoneNumber { get; set; }
        [Required(ErrorMessage = "Enter Valid Email Address")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "Select Logo File")]
        public string LogoFile { get; set; }
        [Required(ErrorMessage = "Select GST No")]
        public string GSTNo { get; set; }
        [Required(ErrorMessage = "Enter PAN No")]
        public string PANNo { get; set; }
        [Required(ErrorMessage = "Enter Telephone No")]
        public string TelNo { get; set; }
        [Required(ErrorMessage = "Enter Bank Name")]
        public string BankName { get; set; }
        [Required(ErrorMessage = "Enter Bank Account No")]
        public string BankAccountNo { get; set; }
        [Required(ErrorMessage = "Enter IFSC Code")]
        public string IFSCCode { get; set; }

        [Required(ErrorMessage = "Enter Bank Address")]
        public string BankAddress { get; set; }
        public string IsActive { get; set; }
        public string CreatedDate { get; set; }
    }
}
