﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using PMS.Utility;

namespace PMS.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        public IActionResult LogIn()
        {

        

                return RedirectToAction("Index", "Auth");

            
        }

        public IActionResult AccessDenied()
        {
            bool isAuthenticated = User.Identity.IsAuthenticated;

       

                return RedirectToAction("Dashboard", "Admin");

            
        }

        
    }
}