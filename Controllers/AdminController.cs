﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PMS.Models;
using System.Linq;
using PMS.Utility;
using System;
using System.Data;
using PMS.Data;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using PMS.Services;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Http;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage;
using System.Text.RegularExpressions;
using System.IO;
using System.Net.Mail;
using OfficeOpenXml;


namespace PMS.Controllers
{
    [AllowAnonymous]
    public class AdminController : Controller
    {



    


        private readonly ILogger<AdminController> _logger;
        private readonly UserManager<ApplicationUser> userManager;
        private readonly SignInManager<ApplicationUser> signInManager;
        private readonly ApplicationDbContext _db;
        private readonly String _connectionString;

        private readonly RoleManager<ApplicationRole> roleManager;
        private readonly IConfiguration _config;
        private readonly CDNDetails CDNSettings;

        [TempData]
        public String Message { get; set; }

        [TempData]
        public String MessageNew { get; set; }
        public object RoundOffType { get; private set; }
        public double RoundOff { get; private set; }

        public AdminController(ILogger<AdminController> logger, UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager, RoleManager<ApplicationRole> roleManager, ApplicationDbContext _db, IConfiguration config, IOptions<CDNDetails> CDNSettings)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.roleManager = roleManager;
            this._db = _db;
            _config = config;
            _logger = logger;
            this.CDNSettings = CDNSettings.Value;
            _connectionString = config.GetConnectionString("DefaultConnection");

        }

        //-----------------------------------------------------------------------CCR-------------------------------------------------------//
        #region 
        public IActionResult Index()
        {
            return View();
        }



        public IActionResult Dashboard()
        {

    
            return View();

        }

        public IActionResult AddTender()
        {
            List<Tender> Priority = new List<Tender>();
            List<Tender> Industry = new List<Tender>();
            List<Tender> TenderCategory = new List<Tender>();
            List<Tender> TenderType = new List<Tender>();
            List<Tender> BiddingType = new List<Tender>();


            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Priority");
            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Priority.Add(new Tender
                {
                    Id = int.Parse(drB["PID"].ToString()),

                    Name = drB["Name"].ToString(),
                    Defination = drB["Defination"].ToString(),
            
                });
            }

            ViewBag.Priority = Priority;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_Industry");
            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Industry.Add(new Tender
                {
                    Id = int.Parse(drB["IID"].ToString()),

                    Name = drB["Name"].ToString(),

                });
            }

            ViewBag.Industry = Industry;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_Category");
            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                TenderCategory.Add(new Tender
                {
                    Id = int.Parse(drB["TID"].ToString()),

                    Name = drB["Name"].ToString(),

                });
            }

            ViewBag.TenderCategory = TenderCategory;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_Type");
            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                TenderType.Add(new Tender
                {
                    Id = int.Parse(drB["TID"].ToString()),

                    Name = drB["Name"].ToString(),

                });
            }

            ViewBag.TenderType = TenderType;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Bidding_Type");
            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                BiddingType.Add(new Tender
                {
                    Id = int.Parse(drB["BID"].ToString()),

                    Name = drB["Name"].ToString(),

                });
            }

            ViewBag.BiddingType = BiddingType;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            return View();

        }



        public IActionResult AddRole()
        {
            HttpContext.Session.Remove("N");


            return View();

        }

        public IActionResult Roles()
        {
            List<ApplicationUser> Roles = new List<ApplicationUser>();



            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Roles");

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Roles.Add(new ApplicationUser
                {
                    Id = int.Parse(drB["Id"].ToString()),
                    FirstName = drB["Name"].ToString() 
       


                });
            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();





            ViewBag.Roles = Roles;

            return View();

        }

        public IActionResult AddEvaluation(int ID)
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Evaluation");
            ViewBag.TenderID = ID;
            return View();

        }

        public IActionResult AddBEC(int ID)
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "BEC");
            ViewBag.Tab = HttpContext.Session.GetString("N");
    
            ViewBag.TenderID = ID;
            return View();

        }





        public IActionResult Tenders() {

            List<Tender> Tenders = new List<Tender>();
            String BS = "";
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            String SS = "";
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            int year = DateTime.Now.Year;
            String CurrentDate = DateTime.Now.ToString("MMMM");
            
            ViewBag.Date = CurrentDate + " " + year;

            DateTime now = DateTime.Now;
            var startDate = new DateTime(now.Year, now.Month, 1);
            var endDate = startDate.AddMonths(1).AddDays(-1);



            ViewBag.StartDate = (startDate.ToString("dd/MM/yyyy")).Replace("-","/");
            ViewBag.EndDate  = endDate.ToString("dd/MM/yyyy").Replace("-", "/");

            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Users_By_ID");
            cmdB.Parameters.AddWithValue("@varID", userId);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                ViewBag.Id = int.Parse(drB["Id"].ToString());
                ViewBag.Email = drB["Email"].ToString();
                ViewBag.PhoneNumber = drB["PhoneNumber"].ToString();
                ViewBag.FirstName = drB["FirstName"].ToString();
                ViewBag.LastName = drB["LastName"].ToString();
                ViewBag.RoleName = drB["RoleName"].ToString();
                ViewBag.Department = drB["Department"].ToString();
                ViewBag.Type = drB["Type"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            if (ViewBag.RoleName == "Admin")
            {
                conn.Open();

                cmdB = new SqlCommand();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Select_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender");

                cmdB.Connection = conn;

                drB = cmdB.ExecuteReader();

                while (drB.Read())
                {
                    if (drB["BECStatus"].ToString() == "Approve")
                    {
                        BS = "Approved";
                    }
                    else if (drB["BECStatus"].ToString() == "Reject")
                    {
                        BS = "Rejected";


                    }
                    else
                    {
                        BS = drB["BECStatus"].ToString();
                    }

                    if (drB["SynopsisStatus"].ToString() == "Approve")
                    {
                        SS = "Approved";
                    }
                    else if (drB["SynopsisStatus"].ToString() == "Reject")
                    {
                        SS = "Rejected";


                    }
                    else
                    {
                        SS = drB["SynopsisStatus"].ToString();
                    }
                    Tenders.Add(new Tender
                    {
                        TenderID = int.Parse(drB["TenderId"].ToString()),

                        ClientName = drB["ClientName"].ToString(),
                        TenderName = drB["TenderName"].ToString(),
                        ScopeOfWork = drB["Scope"].ToString(),
                        Category = drB["Category"].ToString(),
                        Region = drB["Region"].ToString(),
                        Source = drB["Source"].ToString(),
                        SourceDetails = drB["SourceDetails"].ToString(),
                        MonthName = drB["MonthName"].ToString(),
                        TenderNFDate = drB["TenderNFDate"].ToString(),
                        Year = drB["Year"].ToString(),
                        TenderFee = Double.Parse(drB["TenderFee"].ToString()),
                        BECStatus = BS,
                        SynopsisStatus = SS,
                        BillSubOnline = drB["BillSubDateOnline"].ToString(),
                        BillSubOffline = drB["BillSubDateOffline"].ToString(),
                        Status = drB["Status"].ToString(),

                        TenderNo = drB["TenderNo"].ToString(),
                        Remark = drB["Remark"].ToString(),

                        ContactDetails = drB["ContactDetails"].ToString()
                    });
                }

                ViewBag.Tender = Tenders;
                drB.Dispose();
                cmdB.Dispose();
                conn.Close();
            }
            else {

                conn.Open();

                cmdB = new SqlCommand();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Select_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_By_UserID");
                cmdB.Parameters.AddWithValue("@varID", userId);

                cmdB.Connection = conn;

                drB = cmdB.ExecuteReader();

                while (drB.Read())
                {
                    if (drB["BECStatus"].ToString() == "Approve")
                    {
                        BS = "Approved";
                    }
                    else if (drB["BECStatus"].ToString() == "Reject")
                    {
                        BS = "Rejected";


                    }
                    else
                    {
                        BS = drB["BECStatus"].ToString();
                    }

                    if (drB["SynopsisStatus"].ToString() == "Approve")
                    {
                        SS = "Approved";
                    }
                    else if (drB["SynopsisStatus"].ToString() == "Reject")
                    {
                        SS = "Rejected";


                    }
                    else
                    {
                        SS = drB["SynopsisStatus"].ToString();
                    }
                    Tenders.Add(new Tender
                    {
                        TenderID = int.Parse(drB["TenderId"].ToString()),

                        ClientName = drB["ClientName"].ToString(),
                        TenderName = drB["TenderName"].ToString(),
                        ScopeOfWork = drB["Scope"].ToString(),
                        Category = drB["Category"].ToString(),
                        Region = drB["Region"].ToString(),
                        Source = drB["Source"].ToString(),
                        SourceDetails = drB["SourceDetails"].ToString(),

                        TenderNFDate = drB["TenderNFDate"].ToString(),

                        TenderFee = Double.Parse(drB["TenderFee"].ToString()),
                        BECStatus = BS,
                        SynopsisStatus = SS,
                        BillSubOnline = drB["BillSubDateOnline"].ToString(),
                        BillSubOffline = drB["BillSubDateOffline"].ToString(),
                        Status = drB["Status"].ToString(),

                        TenderNo = drB["TenderNo"].ToString(),
                        Remark = drB["Remark"].ToString(),

                        ContactDetails = drB["ContactDetails"].ToString()
                    });
                }

                ViewBag.Tender = Tenders;
                drB.Dispose();
                cmdB.Dispose();
                conn.Close();
            }


            return View();
        }

        [HttpPost]
        public IActionResult Tender_Report_FilterWise()
        {

            List<Tender> Tenders = new List<Tender>();
            String BS = "";
            String SS = "";

            Dblib dblib = new Dblib();

            String startDate = Request.Form["StartDate"];
            String endDate = Request.Form["EndDate"];

            String FinalStartDate = "";
            String FinalEndDate = "";
            if (startDate != "")
            {
                FinalStartDate = dblib.getDate(startDate);
            }

            if (startDate != "")
            {
                FinalEndDate = dblib.getDate(endDate);
            }


            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_By_Date");
            cmdB.Parameters.AddWithValue("@varStartDate", FinalStartDate);
            cmdB.Parameters.AddWithValue("@varEndDate", FinalEndDate);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                if (drB["BECStatus"].ToString() == "Approve")
                {
                    BS = "Approved";
                }
                else if (drB["BECStatus"].ToString() == "Reject")
                {
                    BS = "Rejected";


                }
                else
                {
                    BS = drB["BECStatus"].ToString();
                }

                if (drB["SynopsisStatus"].ToString() == "Approve")
                {
                    SS = "Approved";
                }
                else if (drB["SynopsisStatus"].ToString() == "Reject")
                {
                    SS = "Rejected";


                }
                else
                {
                    SS = drB["SynopsisStatus"].ToString();
                }
                Tenders.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),

                    ClientName = drB["ClientName"].ToString(),
                    TenderName = drB["TenderName"].ToString(),
                    ScopeOfWork = drB["Scope"].ToString(),
                    Category = drB["Category"].ToString(),
                    Region = drB["Region"].ToString(),
                    Source = drB["Source"].ToString(),
                    SourceDetails = drB["SourceDetails"].ToString(),

                    TenderNFDate = drB["TenderNFDate"].ToString(),

                    TenderFee = Double.Parse(drB["TenderFee"].ToString()),
                    BECStatus = BS,
                    SynopsisStatus = SS,
                    BillSubOnline = drB["BillSubDateOnline"].ToString(),
                    BillSubOffline = drB["BillSubDateOffline"].ToString(),
                    Status = drB["Status"].ToString(),

                    TenderNo = drB["TenderNo"].ToString(),
                    Remark = drB["Remark"].ToString(),

                    ContactDetails = drB["ContactDetails"].ToString()
                });
            }

            ViewBag.Tender = Tenders;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            return View("Tenders");
        }



        public IActionResult AddDocument(int ID) {

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Project Docs");
            ViewBag.TenderID = ID;
            return View();
        
        }

        public IActionResult AddChecklist(int ID)
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Checklist");
            ViewBag.TenderID = ID;
            return View();

        }


        public IActionResult AddCommercialDocument(int ID)
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Commercial Docs");
            ViewBag.TenderID = ID;
            return View();

        }


        public async Task<ActionResult> SendMessageAsync(String message, int TenderID) {
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            ApplicationUser user2 = await userManager.FindByIdAsync(userId);
            List<Tender> Communication = new List<Tender>();

            String Name = "-";
            Name = user2.FirstName +" "+user2.LastName;
            if (message != "")
            {
                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();

                    Dblib dblib = new Dblib();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_Communication");
                    cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                    cmdB.Parameters.AddWithValue("@varUserID", userId);
                    cmdB.Parameters.AddWithValue("@varName", Name);
                    cmdB.Parameters.AddWithValue("@varNote", message);



                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();
                    cmdB.Dispose();


                }
                catch (Exception e)
                {
                    Message = "Failed";
                    Console.WriteLine(e.ToString());
                    return RedirectToAction("TenderDetails", new { ID = TenderID });

                }
                finally
                {

                    conn.Close();

                }
            }



       


            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Communication_By_TenderID");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                if (userId == drB["CreatedBy"].ToString())
                {
                    Communication.Add(new Tender
                    {
                        TenderID = int.Parse(drB["TenderId"].ToString()),
                        Id = int.Parse(drB["CID"].ToString()),
                        Name = drB["Name"].ToString(),
                        Note = drB["Message"].ToString(),
                        CreatedDate = drB["CreatedDate"].ToString(),
                        PersonID = int.Parse(drB["CreatedBy"].ToString()),
                        Type = "Self"

                    });
                }
                else
                {
                    Communication.Add(new Tender
                    {
                        TenderID = int.Parse(drB["TenderId"].ToString()),
                        Id = int.Parse(drB["CID"].ToString()),
                        Name = drB["Name"].ToString(),
                        Note = drB["Message"].ToString(),
                        CreatedDate = drB["CreatedDate"].ToString(),
                        PersonID = int.Parse(drB["CreatedBy"].ToString()),
                        Type = "Other"

                    });
                }
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            ViewBag.Communication = Communication;
            return Json(new
            {
                Communication = Communication
            });
        }




        public ActionResult ReadNotification()
        {
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            List<Tender> Communication = new List<Tender>();

                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();

                    Dblib dblib = new Dblib();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Read_Notifications");
                    cmdB.Parameters.AddWithValue("@varID", userId);
          


                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();
                    cmdB.Dispose();


                }
                catch (Exception e)
                {
                Console.WriteLine(e.ToString());                  

                }
                finally
                {

                    conn.Close();

                }

            return Json("OK");
            


        }


        [HttpPost]
        public async Task<IActionResult> UploadProjectFilesAsync()
        {
            
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();


            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Project Docs");
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            var files = HttpContext.Request.Form.Files;

            int TenderID = int.Parse(Request.Form["TenderID"]);
            String File = Request.Form["fileName"];
            String FileType = "application/pdf";
            foreach (var f in files)
            {
                var extentions = f.ContentType;
                if (f.Length < 50000000)
                {

                }
                else
                {
                    Message = "Failed";
                    return RedirectToAction("TenderDetails", new { ID = TenderID });
                }
            }

            IFormFile file = Request.Form.Files.GetFile("doc");


            String FileName = "";
            if (file != null)
            {



                FileName = userId + TenderID + file.Name + file.FileName;

                CloudBlockBlob blob;
                String connStr = CDNSettings.Conn; ;

                CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer container = client.
                              GetContainerReference("pms");
                if (await container.ExistsAsync())
                {
                    FileName = userId + TenderID + file.Name + file.FileName;
                    blob = container.GetBlockBlobReference(FileName);

                    if (await blob.ExistsAsync())
                    {
                        CloudBlockBlob snapshot = container.GetBlockBlobReference(FileName);
                        if (snapshot.SnapshotTime != null)
                        {
                            await blob.DeleteAsync();
                        }

                    }
                }


                using (var memoryStream = file.OpenReadStream())
                {
                    //stream.CopyTo(memoryStream);
                    //imgByte = memoryStream.ToArray();



                    client = account.CreateCloudBlobClient();

                    container = client.
                    GetContainerReference("pms");

                    //container.CreateIfNotExists();

                    blob = container.
                        GetBlockBlobReference(FileName);

                    blob.Properties.ContentType = file.ContentType;

                    FileType = file.ContentType;
                    await blob.UploadFromStreamAsync(memoryStream);



                }

            }


            else
            {
                Message = "Failed";
                return RedirectToAction("TenderDetails", new { ID = TenderID });

            }





            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                Dblib dblib = new Dblib();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Project_Doc");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                cmdB.Parameters.AddWithValue("@varFileName", File); 
                cmdB.Parameters.AddWithValue("@varFileType", FileType);

                
                cmdB.Parameters.AddWithValue("@varContent", FileName);

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();
                cmdB.Dispose();


            }
            catch (Exception e)
            {
                Message = "Failed";
                Console.WriteLine(e.ToString());
                return RedirectToAction("TenderDetails", new { ID = TenderID });

            }
            finally
            {

                conn.Close();

            }
            Message = "Success";

            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }



        public IActionResult SOR() {
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_SOR");

            cmdB.Connection = conn;
            List<Tender> SelfSOR = new List<Tender>();

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                String S = "";
                if (drB["Type"].ToString() == "Self")
                {
                    S = "VCS";
                }
                else {
                    S = drB["Type"].ToString();

                }
                SelfSOR.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["SID"].ToString()),
                    Type = S,
                    TenderRef = drB["TenderRef"].ToString(),
                    CreatedDate = drB["CreatedDate"].ToString(),
                    ClientName = drB["Client"].ToString(),
                    HighLevScope = drB["HighLevScope"].ToString(),
                    Sector = drB["Sector"].ToString(),
                    CostingGroup = drB["CostingGroup"].ToString(),
                    CoverageORDeployment = drB["CoverageORDeployment"].ToString(),
                    CostEst = drB["CostEst"].ToString(),
                    Content = drB["Content"].ToString(),
                    EstimatedValue = drB["EstimatedValue"].ToString(),
                    Status = drB["TenderStatus"].ToString(),
                    CreatedByUser = drB["FirstName"].ToString() + " " + drB["LastName"].ToString(),
                    Name = drB["FileDetails"].ToString()

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.SelfSOR = SelfSOR;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddChecklistDetailsAsync()
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Checklist");
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            var files = HttpContext.Request.Form.Files;

            int TenderID = int.Parse(Request.Form["TenderID"]);
            String Stage = Request.Form["Stage"];
            String TenderDocuments = Request.Form["TenderDocuments"];
            String Remarks = Request.Form["Remarks"];
            int Role = int.Parse(Request.Form["Role"]);
            String FinalStartDate = "";
            Dblib dblib = new Dblib();

            String CompletedByDate = Request.Form["CompletedByDate"];
            String Approval = Request.Form["Approval"];
            int Manager = 0;
            if (!String.IsNullOrEmpty(Request.Form["Manager"]))
            {
                Manager = int.Parse(Request.Form["Manager"]);
            }
            String Status = Request.Form["Status"];

            if (CompletedByDate != "")
            {
                FinalStartDate = dblib.getDate(CompletedByDate);
            }


            String FileType = "application/pdf";
            foreach (var f in files)
            {
                var extentions = f.ContentType;
                if (f.Length < 50000000)
                {

                }
                else
                {
                    Message = "Failed";
                    return RedirectToAction("TenderDetails", new { ID = TenderID });
                }
            }

            IFormFile file = Request.Form.Files.GetFile("doc");


            String FileName = "";
            if (file != null)
            {



                FileName = userId + TenderID + file.Name + file.FileName;

                CloudBlockBlob blob;
                String connStr = CDNSettings.Conn; ;

                CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer container = client.
                              GetContainerReference("pms");
                if (await container.ExistsAsync())
                {
                    FileName = userId + TenderID + file.Name + file.FileName;
                    blob = container.GetBlockBlobReference(FileName);

                    if (await blob.ExistsAsync())
                    {
                        CloudBlockBlob snapshot = container.GetBlockBlobReference(FileName);
                        if (snapshot.SnapshotTime != null)
                        {
                            await blob.DeleteAsync();
                        }

                    }
                }


                using (var memoryStream = file.OpenReadStream())
                {
                    //stream.CopyTo(memoryStream);
                    //imgByte = memoryStream.ToArray();



                    client = account.CreateCloudBlobClient();

                    container = client.
                    GetContainerReference("pms");

                    //container.CreateIfNotExists();

                    blob = container.
                        GetBlockBlobReference(FileName);

                    blob.Properties.ContentType = file.ContentType;

                    FileType = file.ContentType;
                    await blob.UploadFromStreamAsync(memoryStream);



                }

            }








            try
            {
                conn.Open();
                cmdB = new SqlCommand();


                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Checklist");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                cmdB.Parameters.AddWithValue("@varTenderDocuments", TenderDocuments);
                cmdB.Parameters.AddWithValue("@varType", FileType);
                cmdB.Parameters.AddWithValue("@varPersonID", Role);
                cmdB.Parameters.AddWithValue("@varStage", Stage);
                cmdB.Parameters.AddWithValue("@varRemark", Remarks);

                cmdB.Parameters.AddWithValue("@varHod", Approval);
                cmdB.Parameters.AddWithValue("@varCompletedByDate", FinalStartDate);
                cmdB.Parameters.AddWithValue("@varStatus", Status);
                cmdB.Parameters.AddWithValue("@varManagerID", Manager);

                cmdB.Parameters.AddWithValue("@varContent", FileName);

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();
                cmdB.Dispose();


            }
            catch (Exception e)
            {
                Message = "Failed";
                Console.WriteLine(e.ToString());
                return RedirectToAction("TenderDetails", new { ID = TenderID });

            }
            finally
            {

                conn.Close();

            }
            Message = "Success";

            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }


        [HttpPost]
        public async Task<IActionResult> UpdateChecklistDetailsAsync()
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Checklist");
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            var files = HttpContext.Request.Form.Files;

            int TenderID = int.Parse(Request.Form["TenderID"]);
            int CID = int.Parse(Request.Form["CID"]);

            String Stage = Request.Form["Stage"];
            String TenderDocuments = Request.Form["TenderDocuments"];
            
            String Remarks = Request.Form["Remarks"];
            String RemarksByM = Request.Form["RemarksByM"];
            String RemarksByP = Request.Form["RemarksByP"];

            int Role = int.Parse(Request.Form["Role"]);
            String FinalStartDate = "";
            Dblib dblib = new Dblib();

            String CompletedByDate = Request.Form["CompletedByDate"];
            String Approval = Request.Form["Approval"];
            int Manager = 0;
            if (!String.IsNullOrEmpty(Request.Form["Manager"]))
            {
                Manager = int.Parse(Request.Form["Manager"]);
            }
            String Status = Request.Form["Status"];

            if (CompletedByDate != "")
            {
                FinalStartDate = dblib.getDate(CompletedByDate);
            }

            String Stage2 = "";
            String TenderDocuments2 = "";
            String Remarks2 = "";
            int Role2 = 0;
            String FinalStartDate2 = "";
            String CompletedByDate2 = "";
            String Approval2 = "";
            String Status2 = "";
            String FileName = "";
            int Manager2 = 0;

            if (Status == "To Be Revised") { 
              Stage2 = Request.Form["AddStage"];
              TenderDocuments2 = Request.Form["AddTenderDocuments"];
              Remarks2 = Request.Form["AddRemarks"];
              Role2 = int.Parse(Request.Form["AddRole"]);
              CompletedByDate2 = Request.Form["AddCompletedByDate"];
              Approval2 = Request.Form["AddApproval"];
                if (!String.IsNullOrEmpty(Request.Form["AddManager"]))
                {
                    Manager2 = int.Parse(Request.Form["AddManager"]);
                }
                 Status2 = Request.Form["AddStatus"];
                 if (CompletedByDate2 != "")
                    {
                        FinalStartDate2 = dblib.getDate(CompletedByDate2);
                 }


                conn.Open();
                cmdB = new SqlCommand();


                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Checklist");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                cmdB.Parameters.AddWithValue("@varTenderDocuments", TenderDocuments2);
                cmdB.Parameters.AddWithValue("@varType", "-");
                cmdB.Parameters.AddWithValue("@varPersonID", Role2);
                cmdB.Parameters.AddWithValue("@varStage", Stage2);
                cmdB.Parameters.AddWithValue("@varRemark", Remarks2);

                cmdB.Parameters.AddWithValue("@varHod", Approval2);
                cmdB.Parameters.AddWithValue("@varCompletedByDate", FinalStartDate2);
                cmdB.Parameters.AddWithValue("@varStatus", Status2);
                cmdB.Parameters.AddWithValue("@varManagerID", Manager2);

                cmdB.Parameters.AddWithValue("@varContent", "-");

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();
                cmdB.Dispose();
                conn.Close();

            }

            String FileType = "application/pdf";
      

            IFormFile file = Request.Form.Files.GetFile("doc");


            if (file != null)
            {



                FileName = userId + TenderID + file.Name + file.FileName;

                CloudBlockBlob blob;
                String connStr = CDNSettings.Conn; ;

                CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer container = client.
                              GetContainerReference("pms");
                if (await container.ExistsAsync())
                {
                    FileName = userId + TenderID + file.Name + file.FileName;
                    blob = container.GetBlockBlobReference(FileName);

                    if (await blob.ExistsAsync())
                    {
                        CloudBlockBlob snapshot = container.GetBlockBlobReference(FileName);
                        if (snapshot.SnapshotTime != null)
                        {
                            await blob.DeleteAsync();
                        }

                    }
                }


                using (var memoryStream = file.OpenReadStream())
                {
                    //stream.CopyTo(memoryStream);
                    //imgByte = memoryStream.ToArray();



                    client = account.CreateCloudBlobClient();

                    container = client.
                    GetContainerReference("pms");

                    //container.CreateIfNotExists();

                    blob = container.
                        GetBlockBlobReference(FileName);

                    blob.Properties.ContentType = file.ContentType;

                    FileType = file.ContentType;
                    await blob.UploadFromStreamAsync(memoryStream);



                }

            }


            else
            {

            }

            List<Tender> CheckList = new List<Tender>();

            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Checklist_By_ID");
            cmdB.Parameters.AddWithValue("@varID", CID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                CheckList.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["CID"].ToString()),
                    ResponsiblePerson = int.Parse(drB["ResponsiblePerson"].ToString()),

                    Stage = drB["Stage"].ToString(),
                    TenderDocuments = drB["TenderDocuments"].ToString(),
                    CreatedByUser = drB["CreatedFirstName"].ToString() + drB["CreatedLastName"].ToString(),
                    Name = drB["FirstName"].ToString() + drB["LastName"].ToString(),
                    Approval = drB["HodApproval"].ToString(),
                    CompletedByDate = drB["CompltedByDate"].ToString(),
                    Status = drB["Status"].ToString(),
                    Content = drB["Content"].ToString(),
                    Remark = drB["Remark"].ToString(),
                    ManagerID = int.Parse(drB["ManagerID"].ToString()),
                    ManagerName = drB["ManagerFirstName"].ToString() + drB["ManagerLastName"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            if (file != null)
            {

            }
            else {
                FileName = CheckList[0].Content;
            }


            try
            {
                conn.Open();
                cmdB = new SqlCommand();


                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Update_Checklist");
                cmdB.Parameters.AddWithValue("@varID", CID);
                cmdB.Parameters.AddWithValue("@varTenderDocuments", TenderDocuments);
                cmdB.Parameters.AddWithValue("@varType", FileType);
                cmdB.Parameters.AddWithValue("@varPersonID", Role);
                cmdB.Parameters.AddWithValue("@varStage", Stage);
                cmdB.Parameters.AddWithValue("@varRemark", Remarks);
                cmdB.Parameters.AddWithValue("@varRemarkByPerson", RemarksByP);
                cmdB.Parameters.AddWithValue("@varRemarkByManager", RemarksByM);

                cmdB.Parameters.AddWithValue("@varHod", Approval);
                cmdB.Parameters.AddWithValue("@varCompletedByDate", FinalStartDate);
                cmdB.Parameters.AddWithValue("@varStatus", Status);
                cmdB.Parameters.AddWithValue("@varManagerID", Manager);

                cmdB.Parameters.AddWithValue("@varContent", FileName);

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();
                cmdB.Dispose();


            }
            catch (Exception e)
            {
                Message = "Failed";
                Console.WriteLine(e.ToString());
                return RedirectToAction("TenderDetails", new { ID = TenderID });

            }
            finally
            {

                conn.Close();

            }
            Message = "Success";

            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }



        [HttpPost]
        public async Task<IActionResult> UploadCommercialFilesAsync()
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Commercial Docs");

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            var files = HttpContext.Request.Form.Files;

            int TenderID = int.Parse(Request.Form["TenderID"]);
            String File = Request.Form["fileName"];
            String FileType = "application/pdf";
            foreach (var f in files)
            {
                var extentions = f.ContentType;
                if (f.Length < 50000000)
                {

                }
                else
                {
                    Message = "Failed";
                    return RedirectToAction("TenderDetails", new { ID = TenderID });
                }
            }

            IFormFile file = Request.Form.Files.GetFile("doc");


            String FileName = "";
            if (file != null)
            {



                FileName = userId + TenderID + file.Name + file.FileName;

                CloudBlockBlob blob;
                String connStr = CDNSettings.Conn; ;

                CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer container = client.
                              GetContainerReference("pms");
                if (await container.ExistsAsync())
                {
                    FileName = userId + TenderID + file.Name + file.FileName;
                    blob = container.GetBlockBlobReference(FileName);

                    if (await blob.ExistsAsync())
                    {
                        CloudBlockBlob snapshot = container.GetBlockBlobReference(FileName);
                        if (snapshot.SnapshotTime != null)
                        {
                            await blob.DeleteAsync();
                        }

                    }
                }


                using (var memoryStream = file.OpenReadStream())
                {
                    //stream.CopyTo(memoryStream);
                    //imgByte = memoryStream.ToArray();



                    client = account.CreateCloudBlobClient();

                    container = client.
                    GetContainerReference("pms");

                    //container.CreateIfNotExists();

                    blob = container.
                        GetBlockBlobReference(FileName);

                    blob.Properties.ContentType = file.ContentType;

                    FileType = file.ContentType;
                    await blob.UploadFromStreamAsync(memoryStream);



                }

            }


            else
            {
                Message = "Failed";
                return RedirectToAction("TenderDetails", new { ID = TenderID });

            }






            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                Dblib dblib = new Dblib();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Commercial_Doc");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                cmdB.Parameters.AddWithValue("@varFileName", File);
                cmdB.Parameters.AddWithValue("@varFileType", FileType);


                cmdB.Parameters.AddWithValue("@varContent", FileName);

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();
                cmdB.Dispose();


            }
            catch (Exception e)
            {
                Message = "Failed";
                Console.WriteLine(e.ToString());
                return RedirectToAction("TenderDetails", new { ID = TenderID });

            }
            finally
            {

                conn.Close();

            }
            Message = "Success";

            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }
        
        public IActionResult TenderDetails(int ID)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            List<Tender> Tenders = new List<Tender>();
            List<Tender> Queries = new List<Tender>();
            List<Tender> PQueries = new List<Tender>();


            List<Tender> Evaluation = new List<Tender>();
            List<Tender> Evaluation2 = new List<Tender>();
            List<Tender> Evaluation3 = new List<Tender>();

            List<ApplicationUser> User2 = new List<ApplicationUser>();
            List<ApplicationUser> UserToAdd = new List<ApplicationUser>();
            List<Tender> ProjectDocs = new List<Tender>();
            List<Tender> OtherInfo = new List<Tender>();
            List<Tender> BEC = new List<Tender>();
            List<Tender> BEC2 = new List<Tender>();
            List<Tender> BEC3 = new List<Tender>();

            List<Tender> SWOT = new List<Tender>();
            List<Tender> CommercialDocs = new List<Tender>();
            List<Tender> CheckListUser = new List<Tender>();
            List<Tender> CheckListManager = new List<Tender>();
            List<Tender> CheckList = new List<Tender>();
            List<Tender> SelfSOR = new List<Tender>();
            List<Tender> CompetitorSOR = new List<Tender>();
            List<Tender> Communication = new List<Tender>();

            List<Tender> SORDOCS = new List<Tender>();

            List<Tender> MSOR = new List<Tender>();
            List<Tender> TenderWon = new List<Tender>();
            List<Tender> TenderLost = new List<Tender>();
            List<Tender> TenderWonC = new List<Tender>();
            List<Tender> TenderLostC = new List<Tender>();

            ViewBag.Tab = HttpContext.Session.GetString("N");


            ViewBag.ID = ID;
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_By_ID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Tenders.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),

                    ClientName = drB["ClientName"].ToString(),
                    BECStatus = drB["BECStatus"].ToString(),
                    BECRemark = drB["BECRemark"].ToString(),

                    SynopsisStatus = drB["SynopsisStatus"].ToString(),

                    TenderName = drB["TenderName"].ToString(),
                    ScopeOfWork = drB["Scope"].ToString(),
                    Category = drB["Category"].ToString(),
                    Region = drB["Region"].ToString(),
                    Source = drB["Source"].ToString(),
                    SourceDetails = drB["SourceDetails"].ToString(),
                    TenderFeeDetails = drB["TenderFeeDetails"].ToString(),
                    TenderNFDate = drB["TenderNFDate"].ToString(),
                    ContactDetails = drB["ContactDetails"].ToString(),


                    BidValidity = drB["BidValidity"].ToString(),

                    TenderFee = Double.Parse(drB["TenderFee"].ToString()),

                    BidSTimeOnline = drB["BidTimeOl"].ToString(),
                    BidSTimeOffline = drB["BidTimeOf"].ToString(),

                     
                    PreBidDate = drB["PreBidDate"].ToString(),
                    PreBidTime = drB["PreBidTime"].ToString(),
                    BidOpeningDate = drB["BidOpeningDate"].ToString(),

                    BidSDateOnline = drB["BillSubDateOnline"].ToString(),

                    BidSDateOffline = drB["BillSubDateOffline"].ToString(),

                    BidSubmittedOn = drB["BidSubmittedOn"].ToString(),
                    BidOpeningTime = drB["BidOpeningTime"].ToString(),
                    Status = drB["Status"].ToString(),

                    TenderNo = drB["TenderNo"].ToString(),
                    Remark = drB["Remark"].ToString(),
                    
                    TenderType = drB["TenderType"].ToString(),
                    Priority = drB["Priority"].ToString(),
                    PriorityName = drB["PriorityName"].ToString(),
                    CategoryName = drB["TenderCategoryName"].ToString(),
                    BiddingTypeName = drB["BiddingTypeName"].ToString(),
                    TenderTypeName = drB["TenderTypeName"].ToString(),

                    IndustryName = drB["IndustryName"].ToString(),


                    Designation = drB["Designation"].ToString(),

                    Email = drB["Email"].ToString(),
                    PhoneNumber = drB["PhoneNumber"].ToString(),
                    EMD = drB["EMD"].ToString(),
                    EMDE = drB["EMDFlag"].ToString(),
                    TEx = drB["TenderFeeExempted"].ToString(),
                    ContractValue = drB["ContractValue"].ToString(),
                    ContractPeriod = drB["ContractPeriod"].ToString(),
                    ExContractPeriod = drB["ExContractPeriod"].ToString(),
                    Industry = drB["Industry"].ToString(),
                    BiddingType = drB["BiddingType"].ToString(),
                    Mode = drB["Mode"].ToString(),
                    JV = drB["JV"].ToString(),
                    BidAward = drB["BidAward"].ToString()
                });
            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Users_By_ID");
            cmdB.Parameters.AddWithValue("@varID", userId);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                ViewBag.UserId = int.Parse(drB["Id"].ToString());

                ViewBag.Type = drB["Type"].ToString();
                ViewBag.Role = drB["RoleName"].ToString();
                ViewBag.UserName = drB["FirstName"].ToString() + " " + drB["LastName"].ToString();

            }

            Console.WriteLine(ViewBag.Type);
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_TechnicalEvaluation_By_TenderID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Evaluation.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["EID"].ToString()),
                    TenderNo = drB["TenderNo"].ToString(),
                    CriteriaCategory = drB["CriteriaCategory"].ToString(),
                    Criteria = drB["Criteria"].ToString(),
                    AnyOther = drB["OtherCriteria"].ToString(),

                    WeightageMarks = Double.Parse(drB["WeightageMarks"].ToString()),

                    VCSScore = Double.Parse(drB["VCSScore"].ToString()),
                    VCSSatisfied = drB["IsSatisfied"].ToString(),
                    Remark = drB["Remark"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Communication_By_TenderID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                if (userId == drB["CreatedBy"].ToString())
                {
                    Communication.Add(new Tender
                    {
                        TenderID = int.Parse(drB["TenderId"].ToString()),
                        Id = int.Parse(drB["CID"].ToString()),
                        Name = drB["Name"].ToString(),
                        Note = drB["Message"].ToString(),
                        CreatedDate = drB["CreatedDate"].ToString(),
                        PersonID = int.Parse(drB["CreatedBy"].ToString()),
                        Type = "Self"

                    });
                }
                else 
                {
                    Communication.Add(new Tender
                    {
                        TenderID = int.Parse(drB["TenderId"].ToString()),
                        Id = int.Parse(drB["CID"].ToString()),
                        Name = drB["Name"].ToString(),
                        Note = drB["Message"].ToString(),
                        CreatedDate = drB["CreatedDate"].ToString(),
                        PersonID = int.Parse(drB["CreatedBy"].ToString()),
                        Type = "Other"

                    });
                }
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            ViewBag.Communication = Communication;
            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_FinancialEvaluation_By_TenderID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Evaluation2.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["EID"].ToString()),
                    TenderNo = drB["TenderNo"].ToString(),
                    CriteriaCategory = drB["CriteriaCategory"].ToString(),
                    Criteria = drB["Criteria"].ToString(),
                    AnyOther = drB["OtherCriteria"].ToString(),

                    WeightageMarks = Double.Parse(drB["WeightageMarks"].ToString()),

                    VCSScore = Double.Parse(drB["VCSScore"].ToString()),
                    VCSSatisfied = drB["IsSatisfied"].ToString(),
                    Remark = drB["Remark"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_OtherEvaluation_By_TenderID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Evaluation3.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["EID"].ToString()),
                    TenderNo = drB["TenderNo"].ToString(),
                    CriteriaCategory = drB["CriteriaCategory"].ToString(),
                    Criteria = drB["Criteria"].ToString(),
                    AnyOther = drB["OtherCriteria"].ToString(),

                    WeightageMarks = Double.Parse(drB["WeightageMarks"].ToString()),

                    VCSScore = Double.Parse(drB["VCSScore"].ToString()),
                    VCSSatisfied = drB["IsSatisfied"].ToString(),
                    Remark = drB["Remark"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_Won_By_TenderID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                TenderWon.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["TWID"].ToString()),
                    ProjectCode = drB["PID"].ToString(),

                    LOANo = drB["LOANo"].ToString(),
                    ProjectName = drB["ProjectName"].ToString(),
                    Client = drB["Client"].ToString(),
                    ScopeOfWork = drB["ScopeOfWork"].ToString(),

                    TenderCategory = drB["TenderCategory"].ToString(),

                    TenderIndustry = drB["TenderIndustry"].ToString(),
                    ContractValue = drB["ContractValue"].ToString(),
                    Note = drB["Note"].ToString(),
                    IssueDate = drB["IssueDate"].ToString(),
                    StartDate = drB["StartDate"].ToString(),
                    CompletionDate = drB["CompletionDate"].ToString(),


                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_Lost_By_TenderID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                TenderLost.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["TLID"].ToString()),

                    Winner = drB["Winner"].ToString(),
                    WinnerQuotedValue = drB["WinnerQuotedValue"].ToString(),
                    OurQuotedValue = drB["OurQuotedValue"].ToString(),
                    LostBy = drB["LostBy"].ToString(),

                    TenderCategory = drB["TenderCategory"].ToString(),

                    TenderIndustry = drB["TenderIndustry"].ToString(),
                    ReasonForLoss = drB["ReasonForLoss"].ToString(),
                    Note = drB["Note"].ToString(),
     

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Competitor_Analysis_By_TWID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                TenderWonC.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
           

                    Name = drB["NameOfBidder"].ToString(),
                    QuotedPrice = drB["QuotedPrice"].ToString(),
            
                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Competitor_Analysis_By_TLID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                TenderLostC.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),


                    Name = drB["NameOfBidder"].ToString(),
                    QuotedPrice = drB["QuotedPrice"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            ViewBag.TenderWon = TenderWon;
            ViewBag.TenderLost = TenderLost;
            ViewBag.TenderWonC = TenderWonC;

            ViewBag.TenderLostC = TenderLostC;

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_PreBid_Queries");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Queries.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["QID"].ToString()),
                    TenderNo = drB["TenderNo"].ToString(),
                    TenderRef = drB["ReferDocName"].ToString(),
                    Article = drB["Article"].ToString(),
                    ArticleNo = drB["Article"].ToString(),

                    TenderCondition = drB["TenderCondition"].ToString(),
                    VCSQuery = drB["VCSQuery"].ToString(),
                    CustomerResponse = drB["CustomerResponse"].ToString(),
                    CustomerResponseDoc = drB["CustomerResponseDocName"].ToString(),
                    Content = drB["CustomerResponseDocContent"].ToString(),
                    Department = drB["Department"].ToString(),
                    ClarifiedDate = drB["ClarifiedDate"].ToString(),
                    RaisedBy = drB["FirstName"].ToString() + " " + drB["LastName"].ToString(),
                    Status = drB["Status"].ToString(),
                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.Queries = Queries;



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_PostBid_Queries");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                PQueries.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["QID"].ToString()),
                    TenderNo = drB["TenderNo"].ToString(),
                    TenderRef = drB["ReferDocName"].ToString(),
                    Article = drB["Article"].ToString(),
                    ArticleNo = drB["Article"].ToString(),

                    TenderCondition = drB["TenderCondition"].ToString(),
                    VCSQuery = drB["VCSQuery"].ToString(),
                    CustomerResponse = drB["CustomerResponse"].ToString(),
                    CustomerResponseDoc = drB["CustomerResponseDocName"].ToString(),
                    Content = drB["CustomerResponseDocContent"].ToString(),
                    Department = drB["Department"].ToString(),
                    ClarifiedDate = drB["ClarifiedDate"].ToString(),
                    RaisedBy = drB["FirstName"].ToString() + " " + drB["LastName"].ToString(),
                    Status = drB["Status"].ToString(),
                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.PQueries = PQueries;


            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_SWOT_By_TenderID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                SWOT.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["SID"].ToString()),
                    Strength = drB["Strength"].ToString(),
                    Weakness = drB["Weakness"].ToString(),
                    CreatedByUser = drB["FirstName"].ToString() +" "+ drB["LastName"].ToString(),
                    CreatedDate = drB["CreatedDate"].ToString(),

                    Oppurtunities = drB["Oppurtunities"].ToString(),
                    Threats = drB["Threats"].ToString(),

                }) ;
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            ViewBag.PID = 0;

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_Project");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
               ViewBag.PID = int.Parse(drB["PID"].ToString());
      
            }
            


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Self_SOR");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                SelfSOR.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["SID"].ToString()),
                    Type = drB["Type"].ToString(),
                    TenderRef = drB["TenderRef"].ToString(),
                    CreatedDate = drB["CreatedDate"].ToString(),
                    ClientName = drB["Client"].ToString(),
                    HighLevScope = drB["HighLevScope"].ToString(),
                    Sector = drB["Sector"].ToString(),
                    CostingGroup = drB["CostingGroup"].ToString(),
                    CoverageORDeployment = drB["CoverageORDeployment"].ToString(),
                    CostEst = drB["CostEst"].ToString(),
                    Content = drB["Content"].ToString(),
                    EstimatedValue = drB["EstimatedValue"].ToString(),
                    Status = drB["TenderStatus"].ToString(),
                    FileDetails = drB["FileDetails"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_MSOR");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                MSOR.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["SID"].ToString()),
                    Type = drB["Type"].ToString(),
                    TenderRef = drB["TenderRef"].ToString(),
                    CreatedDate = drB["CreatedDate"].ToString(),
                    ClientName = drB["Client"].ToString(),
                    HighLevScope = drB["HighLevScope"].ToString(),
                    Sector = drB["Sector"].ToString(),
                    CostingGroup = drB["CostingGroup"].ToString(),
                    CoverageORDeployment = drB["CoverageORDeployment"].ToString(),
                    CostEst = drB["CostEst"].ToString(),
                    Content = drB["Content"].ToString(),
                    EstimatedValue = drB["EstimatedValue"].ToString(),
                    Status = drB["TenderStatus"].ToString(),
                    FileDetails = drB["FileDetails"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            ViewBag.MSOR = MSOR;

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Competitor_SOR");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                CompetitorSOR.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["SID"].ToString()),
                    CompetitorName = drB["CompetitorName"].ToString(),
                    Type = drB["Type"].ToString(),
                    TenderRef = drB["TenderRef"].ToString(),
                    CreatedDate = drB["CreatedDate"].ToString(),
                    ClientName = drB["Client"].ToString(),
                    HighLevScope = drB["HighLevScope"].ToString(),
                    Sector = drB["Sector"].ToString(),
                    CostingGroup = drB["CostingGroup"].ToString(),
                    CoverageORDeployment = drB["CoverageORDeployment"].ToString(),
                    CostEst = drB["CostEst"].ToString(),
                    Content = drB["Content"].ToString(),
                    EstimatedValue = drB["EstimatedValue"].ToString(),
                    Status = drB["TenderStatus"].ToString(),
                    FileDetails = drB["FileDetails"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.CompetitorSOR = CompetitorSOR;


            ViewBag.SelfSOR = SelfSOR;

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_PredBidChecklist");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                CheckList.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["CID"].ToString()),
                    Stage = drB["Stage"].ToString(),
                    TenderDocuments = drB["TenderDocuments"].ToString(),
                    CreatedByUser = drB["CreatedFirstName"].ToString() + " " + drB["CreatedLastName"].ToString(),
                    Name = drB["FirstName"].ToString() + " " + drB["LastName"].ToString(),
                    Approval = drB["HodApproval"].ToString(),
                    CompletedByDate = drB["CompltedByDate"].ToString(),
                    Status = drB["Status"].ToString(),
                    Content = drB["Content"].ToString(),
                    Remark = drB["Remark"].ToString(),
                    ManagerID = int.Parse(drB["ManagerID"].ToString()),
                    ManagerName = drB["ManagerFirstName"].ToString() + " " + drB["ManagerLastName"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            List<Tender> PostBid = new List<Tender>();

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_PostBidChecklist");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                PostBid.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["CID"].ToString()),
                    Stage = drB["Stage"].ToString(),
                    TenderDocuments = drB["TenderDocuments"].ToString(),
                    CreatedByUser = drB["CreatedFirstName"].ToString() + " " + drB["CreatedLastName"].ToString(),
                    Name = drB["FirstName"].ToString() + " " + drB["LastName"].ToString(),
                    Approval = drB["HodApproval"].ToString(),
                    CompletedByDate = drB["CompltedByDate"].ToString(),
                    Status = drB["Status"].ToString(),
                    Content = drB["Content"].ToString(),
                    Remark = drB["Remark"].ToString(),
                    ManagerID = int.Parse(drB["ManagerID"].ToString()),
                    ManagerName = drB["ManagerFirstName"].ToString() + " " + drB["ManagerLastName"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            ViewBag.PostBid = PostBid;

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Checklist_Users_By_Tender_ID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                CheckListUser.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["Id"].ToString()),
                    FirstName = drB["FirstName"].ToString(),
                    LastName = drB["LastName"].ToString(),
                    Type = drB["Type"].ToString(),

                    Department = drB["DepartmentName"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            Console.WriteLine(CheckListUser.Count);

            ViewBag.CheckListUser = CheckListUser;




            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Checklist_Users_By_Tender_ID_Manager");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                CheckListManager.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["Id"].ToString()),
                    FirstName = drB["FirstName"].ToString(),
                    LastName = drB["LastName"].ToString(),
                    Type = drB["Type"].ToString(),

                    Department = drB["DepartmentName"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            ViewBag.CheckListManager = CheckListManager;

            ViewBag.SWOT = SWOT;


            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_TechnicalBEC_By_TenderID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);


            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                BEC.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["BID"].ToString()),
                    TenderNo = drB["TenderNo"].ToString(),
                    CriteriaCategory = drB["CriteriaCategory"].ToString(),
                    Criteria = drB["Criteria"].ToString(),
                    AnyOther = drB["OtherCriteria"].ToString(),

                    Status = drB["Status"].ToString(),
                    VCSSatisfied = drB["IsSatisfied"].ToString(),
                    Remark = drB["Remark"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.BEC = BEC;


            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_FinancialBEC_By_TenderID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);


            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                BEC2.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["BID"].ToString()),
                    TenderNo = drB["TenderNo"].ToString(),
                    CriteriaCategory = drB["CriteriaCategory"].ToString(),
                    Criteria = drB["Criteria"].ToString(),
                    AnyOther = drB["OtherCriteria"].ToString(),

                    Status = drB["Status"].ToString(),
                    VCSSatisfied = drB["IsSatisfied"].ToString(),
                    Remark = drB["Remark"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.BEC2 = BEC2;


            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_OtherBEC_By_TenderID");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);


            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                BEC3.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["BID"].ToString()),
                    TenderNo = drB["TenderNo"].ToString(),
                    CriteriaCategory = drB["CriteriaCategory"].ToString(),
                    Criteria = drB["Criteria"].ToString(),
                    AnyOther = drB["OtherCriteria"].ToString(),

                    Status = drB["Status"].ToString(),
                    VCSSatisfied = drB["IsSatisfied"].ToString(),
                    Remark = drB["Remark"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.BEC3 = BEC3;


            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Project_Docs_By_TenderID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                ProjectDocs.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["PID"].ToString()),
                    Name = drB["Name"].ToString(),
                    Content = drB["Content"].ToString(),
                    CreatedDate = drB["CreatedDate"].ToString(),
                    CreatedByUser = drB["FirstName"].ToString() + " "+ drB["LastName"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_SOR_DOCS");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                SORDOCS.Add(new Tender
                {
                    Type = drB["Type"].ToString(),

                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["SID"].ToString()),
                    Name = drB["FileDetails"].ToString(),
                    Content = drB["Content"].ToString(),
                    CreatedDate = drB["CreatedDate"].ToString(),
                    CreatedByUser = drB["FirstName"].ToString() + " " + drB["LastName"].ToString(),
                    Status = drB["TenderStatus"].ToString(),
                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            ViewBag.SORDOCS = SORDOCS;
            Console.WriteLine("S" + SORDOCS.Count);
            ViewBag.ProjectDocs = ProjectDocs;



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Commercial_Docs_By_TenderID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                CommercialDocs.Add(new Tender
                {
                    Type = "Commercial Docs",
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["CID"].ToString()),
                    Name = drB["Name"].ToString(),
                    Content = drB["Content"].ToString(),
                    CreatedDate = drB["CreatedDate"].ToString(),
                    CreatedByUser = drB["FirstName"].ToString() + " " + drB["LastName"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            ViewBag.CommercialDocs = CommercialDocs;

            conn.Open();
             cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Users_By_TenderID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                if (drB["Id"].ToString() != userId)
                {
                    User2.Add(new ApplicationUser
                    {
                        Id = int.Parse(drB["Id"].ToString()),
                        Email = drB["Email"].ToString(),
                        PhoneNumber = drB["PhoneNumber"].ToString(),
                        FirstName = drB["FirstName"].ToString() + " " + drB["LastName"].ToString(),
                        LastName = drB["RoleName"].ToString(),
                        //Department = drB["Department"].ToString()


                    });
                }
            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_UsersToAdd_By_TenderID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                UserToAdd.Add(new ApplicationUser
                {
                    Id = int.Parse(drB["Id"].ToString()),
                    Email = drB["Email"].ToString(),
                    PhoneNumber = drB["PhoneNumber"].ToString(),
                    FirstName = drB["FirstName"].ToString() + " " + drB["LastName"].ToString(),
                    LastName = drB["RoleName"].ToString(),
                    Department = drB["Department"].ToString()


                });
            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();






            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Other_Info");
            cmdB.Parameters.AddWithValue("@varTenderID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                OtherInfo.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["InfoId"].ToString()),
                    ManpowerDetails = drB["ManpowerDetails"].ToString(),
                    ManpowerQualification = drB["ManpowerQualification"].ToString(),
                    EquipmentConsumables = drB["EquipmentConsumables"].ToString(),


                    TransportationRequirement = drB["TransportationRequirement"].ToString(),

                    OfficeSetup = drB["OfficeSetup"].ToString(),
                    GuestHouse = drB["GuestHouse"].ToString(),



                    MobPeriod = drB["MobPeriod"].ToString(),
                    MobAdvance = drB["MobAdvance"].ToString(),
                    SecurityDeposit = drB["SecurityDeposit"].ToString(),


                    DefaultPeriod = drB["DefaultPeriod"].ToString(),

                    Penalities = drB["Penalities"].ToString(),
                    PaymentTerms = drB["PaymentTerms"].ToString(),




                    LiquidityDamage = drB["LiquidityDamage"].ToString(),
                    Insurance = drB["Insurance"].ToString(),
                    PriceEscalation = drB["PriceEscalation"].ToString(),


                    PurchasePreference = drB["PurchasePreference"].ToString(),

                    StartUpClause = drB["StartUpClause"].ToString(),
                    ParentCompanyExperience = drB["ParentCompanyExperience"].ToString(),


                    Competitors = drB["Competitors"].ToString(),

                    AnyOther = drB["AnyOther"].ToString(),


                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            ViewBag.OtherInfo = OtherInfo;

            ViewBag.Users = User2;
            ViewBag.UserToAdd = UserToAdd; 



            ViewBag.Tender = Tenders;
            ViewBag.Evaluation = Evaluation;
            ViewBag.Evaluation2 = Evaluation2;
            ViewBag.Evaluation3 = Evaluation3;


            ViewBag.CheckList = CheckList;


            List<Tender> Priority = new List<Tender>();
            List<Tender> Industry = new List<Tender>();
            List<Tender> TenderCategory = new List<Tender>();
            List<Tender> TenderType = new List<Tender>();
            List<Tender> BiddingType = new List<Tender>();


            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Priority");
            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Priority.Add(new Tender
                {
                    Id = int.Parse(drB["PID"].ToString()),

                    Name = drB["Name"].ToString(),
                    Defination = drB["Defination"].ToString(),

                });
            }

            ViewBag.Priority = Priority;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_Industry");
            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Industry.Add(new Tender
                {
                    Id = int.Parse(drB["IID"].ToString()),

                    Name = drB["Name"].ToString(),

                });
            }

            ViewBag.Industry = Industry;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_Category");
            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                TenderCategory.Add(new Tender
                {
                    Id = int.Parse(drB["TID"].ToString()),

                    Name = drB["Name"].ToString(),

                });
            }

            ViewBag.TenderCategory = TenderCategory;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_Type");
            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                TenderType.Add(new Tender
                {
                    Id = int.Parse(drB["TID"].ToString()),

                    Name = drB["Name"].ToString(),

                });
            }

            ViewBag.TenderType = TenderType;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Bidding_Type");
            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                BiddingType.Add(new Tender
                {
                    Id = int.Parse(drB["BID"].ToString()),

                    Name = drB["Name"].ToString(),

                });
            }

            ViewBag.BiddingType = BiddingType;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            return View();
        }


        public IActionResult ViewDocument(int ID) {
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Project Docs");

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Project_Docs_By_ID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

           SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                ViewBag.TenderID = int.Parse(drB["TenderId"].ToString());
                ViewBag.ID = int.Parse(drB["PID"].ToString());
                ViewBag.Name = drB["Name"].ToString();
                ViewBag.Content = drB["Content"].ToString();
                ViewBag.Type = drB["Type"].ToString();
                ViewBag.CreatedDate = drB["CreatedDate"].ToString();
                ViewBag.CreatedByUser = drB["FirstName"].ToString() + " " + drB["LastName"].ToString();

            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            return View();
        }



        public IActionResult ViewCommercialDocument(int ID)
        {
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Commercial Docs");

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Commercial_Docs_By_ID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                ViewBag.TenderID = int.Parse(drB["TenderId"].ToString());
                ViewBag.ID = int.Parse(drB["CID"].ToString());
                ViewBag.Name = drB["Name"].ToString();
                ViewBag.Content = drB["Content"].ToString();
                ViewBag.CreatedDate = drB["CreatedDate"].ToString();
                ViewBag.CreatedByUser = drB["FirstName"].ToString() + " " + drB["LastName"].ToString();

            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            return View();
        }




        public async Task<IActionResult> DeleteFile(int? ID, int P)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();


            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Project Docs");

            String Content = "";
            try
            {
                conn.Open();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Delete_Tender_Proj_Docs");

                cmdB.Parameters.AddWithValue("@varID", ID);

                cmdB.Connection = conn;

                cmdB.ExecuteNonQuery();
                cmdB.Dispose();
            }
            catch (Exception e)
            {
                Message = "Failed";
                Console.WriteLine(e.ToString());
                return RedirectToAction("TenderDetails", new { ID = P });

            }
            finally
            {

                conn.Close();
            }

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Project_Docs_For_Delete");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                Content = drB["Content"].ToString();
            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            CloudBlockBlob blob;
            String connStr = CDNSettings.Conn;

            CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

            CloudBlobClient client = account.CreateCloudBlobClient();
            CloudBlobContainer container = client.
            GetContainerReference("pms");
            if (await container.ExistsAsync())
            {
                blob = container.GetBlockBlobReference(Content);

                if (await blob.ExistsAsync())
                {
                    CloudBlockBlob snapshot = container.GetBlockBlobReference(Content);
                    if (snapshot.SnapshotTime != null)
                    {
                        await blob.DeleteAsync();
                    }

                }
            }

            Message = "Success";

            return RedirectToAction("TenderDetails", new { ID = P });
        }


        public async Task<IActionResult> DeleteCommercialFile(int? ID, int P)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();


            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Project Docs");

            String Content = "";
            try
            {
                conn.Open();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Delete_Tender_Commercial_Docs");

                cmdB.Parameters.AddWithValue("@varID", ID);

                cmdB.Connection = conn;

                cmdB.ExecuteNonQuery();
                cmdB.Dispose();
            }
            catch (Exception e)
            {
                Message = "Failed";
                Console.WriteLine(e.ToString());
                return RedirectToAction("TenderDetails", new { ID = P });

            }
            finally
            {

                conn.Close();
            }

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Project_Docs_For_Delete");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                Content = drB["Content"].ToString();
            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            CloudBlockBlob blob;
            String connStr = CDNSettings.Conn;

            CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

            CloudBlobClient client = account.CreateCloudBlobClient();
            CloudBlobContainer container = client.
            GetContainerReference("pms");
            if (await container.ExistsAsync())
            {
                blob = container.GetBlockBlobReference(Content);

                if (await blob.ExistsAsync())
                {
                    CloudBlockBlob snapshot = container.GetBlockBlobReference(Content);
                    if (snapshot.SnapshotTime != null)
                    {
                        await blob.DeleteAsync();
                    }

                }
            }

            Message = "Success";

            return RedirectToAction("TenderDetails", new { ID = P });
        }



        public IActionResult ViewEvaluation(int ID) {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Evaluation");

            List<Tender> Evaluation = new List<Tender>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            conn.Open();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Evaluation_By_EID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

           SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Evaluation.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["EID"].ToString()),
                    TenderNo = drB["TenderNo"].ToString(),
                    CriteriaCategory = drB["CriteriaCategory"].ToString(),
                    Criteria = drB["Criteria"].ToString(),
                    AnyOther = drB["OtherCriteria"].ToString(),

                    WeightageMarks = Double.Parse(drB["WeightageMarks"].ToString()),

                    VCSScore = Double.Parse(drB["VCSScore"].ToString()),
                    VCSSatisfied = drB["IsSatisfied"].ToString(),
                    Remark = drB["Remark"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.Evaluation = Evaluation;
            return View();

        }



        public IActionResult ViewQuery(int ID) {
            List<Tender> Query = new List<Tender>();

            HttpContext.Session.Remove("N");
            HttpContext.Session.SetString("N", "Queries");


            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Queries_By_ID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Query.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["QID"].ToString()),
                    TenderNo = drB["TenderNo"].ToString(),
                    TenderRef = drB["ReferDocName"].ToString(),
                    Article = drB["Article"].ToString(),
                    ArticleNo = drB["Article"].ToString(),

                    TenderCondition = drB["TenderCondition"].ToString(),
                    VCSQuery = drB["VCSQuery"].ToString(),
                    CustomerResponse = drB["CustomerResponse"].ToString(),
                    CustomerResponseDoc = drB["CustomerResponseDocName"].ToString(),
                    Content = drB["CustomerResponseDocContent"].ToString(),
                    Department = drB["Department"].ToString(),
                    ClarifiedDate = drB["ClarifiedDate"].ToString(),
                    RaisedBy = drB["FirstName"].ToString() + " " + drB["LastName"].ToString(),
                    Status = drB["Status"].ToString(),
                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.Query = Query;
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId


            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Users_By_ID");
            cmdB.Parameters.AddWithValue("@varID", userId);

            cmdB.Connection = conn;

           drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                ViewBag.Id = int.Parse(drB["Id"].ToString());
                ViewBag.Email = drB["Email"].ToString();
                ViewBag.PhoneNumber = drB["PhoneNumber"].ToString();
                ViewBag.FirstName = drB["FirstName"].ToString();
                ViewBag.LastName = drB["LastName"].ToString();
                ViewBag.RoleName = drB["RoleName"].ToString();
                ViewBag.Department = drB["Department"].ToString();
                ViewBag.Type = drB["Type"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            return View();

        }

        public IActionResult ViewChecklist(int ID)
        {

            List<Tender> CheckList = new List<Tender>();
            List<Tender> CheckListUser = new List<Tender>();
            List<Tender> CheckListManager = new List<Tender>();

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Checklist");


            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            conn.Open();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Checklist_By_ID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                CheckList.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["CID"].ToString()),
                    ResponsiblePerson = int.Parse(drB["ResponsiblePerson"].ToString()),

                    Stage = drB["Stage"].ToString(),
                    TenderDocuments = drB["TenderDocuments"].ToString(),
                    CreatedByUser = drB["CreatedFirstName"].ToString() + drB["CreatedLastName"].ToString(),
                    Name = drB["FirstName"].ToString() + drB["LastName"].ToString(),
                    Approval = drB["HodApproval"].ToString(),
                    CompletedByDate = drB["CompltedByDate"].ToString(),
                    Status = drB["Status"].ToString(),
                    Content = drB["Content"].ToString(),
                    Remark = drB["Remark"].ToString(),
                    ManagerID = int.Parse(drB["ManagerID"].ToString()),
                    RemarksByP = drB["RemarkByPerson"].ToString(),
                    RemarksByM = drB["RemarkByManager"].ToString(),

                    ManagerName = drB["ManagerFirstName"].ToString() + drB["ManagerLastName"].ToString(),

                }) ;
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.CheckList = CheckList;



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Checklist_Users_By_Tender_ID");
            cmdB.Parameters.AddWithValue("@varTenderID", CheckList[0].TenderID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                CheckListUser.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["Id"].ToString()),
                    FirstName = drB["FirstName"].ToString(),
                    LastName = drB["LastName"].ToString(),
                    Type = drB["Type"].ToString(),

                    Department = drB["DepartmentName"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            Console.WriteLine(CheckListUser.Count);

            ViewBag.CheckListUser = CheckListUser;




            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Checklist_Users_By_Tender_ID_Manager");
            cmdB.Parameters.AddWithValue("@varTenderID", CheckList[0].TenderID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                CheckListManager.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["Id"].ToString()),
                    FirstName = drB["FirstName"].ToString(),
                    LastName = drB["LastName"].ToString(),
                    Type = drB["Type"].ToString(),

                    Department = drB["DepartmentName"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            ViewBag.CheckListManager = CheckListManager;
            return View();

        }


        public IActionResult ViewBEC(int ID)
        {
            List<Tender> BEC = new List<Tender>();


            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "BEC");

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            conn.Open();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_BEC_By_ID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                BEC.Add(new Tender
                {
                    TenderID = int.Parse(drB["TenderId"].ToString()),
                    Id = int.Parse(drB["BID"].ToString()),
                    TenderNo = drB["TenderNo"].ToString(),
                    CriteriaCategory = drB["CriteriaCategory"].ToString(),
                    Criteria = drB["Criteria"].ToString(),
                    AnyOther = drB["OtherCriteria"].ToString(),


                    VCSSatisfied = drB["IsSatisfied"].ToString(),
                    Remark = drB["Remark"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.BEC = BEC;
            return View();

        }



        [HttpPost]
        public IActionResult UpdateEvaluationDetails()
        {
            List<Tender> Evaluation = new List<Tender>();


            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Evaluation");


            int EID = int.Parse(Request.Form["EID"]);

            int TenderID = int.Parse(Request.Form["TenderID"]);


            String CriteriaCategory = Request.Form["CriteriaCategory"];


            String Criteria = Request.Form["Criteria"];

            String WeightageMarks = Request.Form["WeightageMarks"];
            String VCSScore = Request.Form["VCSScore"];

            String Satisfied = Request.Form["Satisfied"];

            String Remark = Request.Form["Remark"];
            String Other = "";
            if (CriteriaCategory == "Other")
            {

                Other = Request.Form["Other"];
            }

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            conn.Open();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_Evaluation");
            cmdB.Parameters.AddWithValue("@varID", EID);


            cmdB.Parameters.AddWithValue("@varCriteriaCategory", CriteriaCategory);
            cmdB.Parameters.AddWithValue("@varCriteria", Criteria);
            cmdB.Parameters.AddWithValue("@varWeightageMarks", WeightageMarks);
            cmdB.Parameters.AddWithValue("@varOther", Other);

            cmdB.Parameters.AddWithValue("@varVCSScore", VCSScore);
            cmdB.Parameters.AddWithValue("@varIsSatisfied", Satisfied);
            cmdB.Parameters.AddWithValue("@varRemark", Remark);


            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();


            cmdB.Dispose();
            conn.Close();
            ViewBag.Evaluation = Evaluation;
            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }


        [HttpPost]
        public IActionResult AddTenderWin() {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Post Award Analysis");
            String[] txt_note = Request.Form["NameB[]"];
            String[] txt_note2 = Request.Form["QPrice[]"];
            int cnt = txt_note.Length;

            int TenderID = int.Parse(Request.Form["TenderID"]);

            String ProjectCode = Request.Form["ProjectCode"];
            String LOANO = Request.Form["LOANO"];
            String ProjectName = Request.Form["ProjectName"];
            String Client = Request.Form["Client"];

            String ContractValue = Request.Form["ContractValue"];
            String ScopeOfWork = Request.Form["ScopeOfWork"];
            String TenderCategory = Request.Form["TenderCategory"];
            String Industry = Request.Form["Industry"];
            String Note = Request.Form["Note"];
            String Date = Request.Form["Date"];
            String Status = Request.Form["Status"];
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            int WinID = 0;
            int LID = 0;

            String FinalIssueDate = "";
            String FinalStartDate = "";
            String FinalCompletionDate = "";
            Dblib dblib = new Dblib();

            String IssueDate = Request.Form["IssueDate"];
            if (IssueDate != "") {
                FinalIssueDate = dblib.getDate(IssueDate);

            }

            String StartDate = Request.Form["StartDate"];
            if (StartDate != "") {
                FinalStartDate = dblib.getDate(StartDate);
            }
            String CompletionDate = Request.Form["CompletionDate"];
            if (CompletionDate != "") {
                FinalCompletionDate = dblib.getDate(CompletionDate);
            }

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            try
            {
                conn.Open();
                SqlCommand cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Tender_Won");
                cmdB.Parameters.AddWithValue("@varPID", ProjectCode);
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varLOA", LOANO);
                cmdB.Parameters.AddWithValue("@varProjectName", ProjectName);
                cmdB.Parameters.AddWithValue("@varClientName", Client);
                cmdB.Parameters.AddWithValue("@varScope", ScopeOfWork);
                cmdB.Parameters.AddWithValue("@varTenderCategoryName", TenderCategory);
                cmdB.Parameters.AddWithValue("@varTenderIndustryName", Industry);
                cmdB.Parameters.AddWithValue("@varNote", Note);
                cmdB.Parameters.AddWithValue("@varContractValue", ContractValue);
                cmdB.Parameters.AddWithValue("@varIssueDate", FinalIssueDate);
                cmdB.Parameters.AddWithValue("@varStartDate", FinalStartDate);
                cmdB.Parameters.AddWithValue("@varCompletionDate", FinalCompletionDate);
                cmdB.Parameters.AddWithValue("@varUserID", userId);

                cmdB.Connection = conn;
                SqlDataReader sdr = cmdB.ExecuteReader();
                while (sdr.Read())
                {
                    WinID = int.Parse(sdr["TWID"].ToString());
                }
                cmdB.Dispose();
                sdr.Dispose();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
            finally {
                conn.Close();
            }

            if (WinID != 0)
            {
                try
                {
                    for (int i = 0; i < cnt; i++)
                    {
                        conn.Open();
                        SqlCommand cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_PMS_Add_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_Analysis");
                        cmdB.Parameters.AddWithValue("@varUserID", userId);
                        cmdB.Parameters.AddWithValue("@varTWID", WinID);
                        cmdB.Parameters.AddWithValue("@varTLID", LID);
                        cmdB.Parameters.AddWithValue("@varTenderID", TenderID);

                        cmdB.Parameters.AddWithValue("@varName", txt_note[i]);
                        cmdB.Parameters.AddWithValue("@varQuotedPrice", txt_note2[i]);

                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                        cmdB.Dispose();
                        conn.Close();

                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally {
                }
            }


            return RedirectToAction("TenderDetails", new { ID = TenderID });
        }



        [HttpPost]
        public IActionResult AddTenderLost()
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Post Award Analysis");
            String[] txt_note = Request.Form["NameB[]"];
            String[] txt_note2 = Request.Form["QPrice[]"];
            int cnt = txt_note.Length;

            int TenderID = int.Parse(Request.Form["TenderID"]);

            String NameOfW = Request.Form["NameOfW"];
            String WQValue = Request.Form["WQValue"];
            String OQValue = Request.Form["OQValue"];
            String LostV = Request.Form["LostV"];
            String ReasonForLoss = Request.Form["Reason"];


            String TenderCategory = Request.Form["TenderCategory"];
            String Industry = Request.Form["Industry"];
            String Note = Request.Form["Note"];

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            int WinID = 0;
            int LID = 0;

      
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            try
            {
                conn.Open();
                SqlCommand cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Tender_Lost");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varWinner", NameOfW);
                cmdB.Parameters.AddWithValue("@varWinnerQuotedValue", WQValue);
                cmdB.Parameters.AddWithValue("@varOurQuotedValue", OQValue);
                cmdB.Parameters.AddWithValue("@varTenderCategoryName", TenderCategory);
                cmdB.Parameters.AddWithValue("@varTenderIndustryName", Industry);
                cmdB.Parameters.AddWithValue("@varNote", Note);
                cmdB.Parameters.AddWithValue("@varLostBy", LostV);

                cmdB.Parameters.AddWithValue("@varReasonForLoss", ReasonForLoss);

                cmdB.Parameters.AddWithValue("@varUserID", userId);

                cmdB.Connection = conn;
                SqlDataReader sdr = cmdB.ExecuteReader();
                while (sdr.Read())
                {
                    WinID = int.Parse(sdr["TLID"].ToString());
                }
                cmdB.Dispose();
                sdr.Dispose();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
            finally
            {
                conn.Close();
            }

            if (WinID != 0)
            {
                try
                {
                    for (int i = 0; i < cnt; i++)
                    {
                        conn.Open();
                        SqlCommand cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_PMS_Add_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_Analysis");
                        cmdB.Parameters.AddWithValue("@varUserID", userId);
                        cmdB.Parameters.AddWithValue("@varTLID", WinID);
                        cmdB.Parameters.AddWithValue("@varTWID", LID);
                        cmdB.Parameters.AddWithValue("@varTenderID", TenderID);

                        cmdB.Parameters.AddWithValue("@varName", txt_note[i]);
                        cmdB.Parameters.AddWithValue("@varQuotedPrice", txt_note2[i]);

                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                        cmdB.Dispose();
                        conn.Close();

                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {
                }
            }


            return RedirectToAction("TenderDetails", new { ID = TenderID });
        }

        [HttpPost]
        public IActionResult SubmitTenderStatus()
        {


            //HttpContext.Session.Remove("N");

            //HttpContext.Session.SetString("N", "Evaluation");

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            int TenderID = int.Parse(Request.Form["TenderID"]);
            String[] UserName = Request.Form["UserName[]"];
            String TenderStatus = Request.Form["TenderStatus"];

            String TenderName = "";
            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_By_ID");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                TenderName = drB["TenderName"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            int cnt = UserName.Length;
            String email = "";
            String fullName = "";
            for (int i = 0; i < cnt; i++) {


                conn.Open();
                cmdB = new SqlCommand();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Select_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Select_Users_By_ID");
                cmdB.Parameters.AddWithValue("@varID", UserName[i]);

                cmdB.Connection = conn;

                drB = cmdB.ExecuteReader();

                while (drB.Read())
                {
                    email = drB["Email"].ToString();
                    fullName = drB["FirstName"].ToString() + " " + drB["LastName"].ToString();


                }

                drB.Dispose();
                cmdB.Dispose();
                conn.Close();


                using (var memoryStream = new MemoryStream())
                {
                    MailMessage mail = new MailMessage();
                    mail.To.Add(email);
                    mail.From = new MailAddress("vcsnashik@gmail.com");
                    mail.Subject = "Tender Status";
                    mail.Body = "Hello " + fullName + ", <br /><br />Tender "+ TenderName + " is "+TenderStatus+".  <br /> <br /><br />Regards, <br />Team VCS";
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                    smtp.EnableSsl = true;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential("vcsnashik@gmail.com", "VCS@2021");
                    smtp.Send(mail);
                }
            }



         
            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_Tender_Status");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
            cmdB.Parameters.AddWithValue("@varStatus", TenderStatus);
            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();

          


            cmdB.Dispose();
            conn.Close();

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId


            if (TenderStatus == "Won")
            {

                conn.Open();
                cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Tender_Project");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varUserID", userId);
                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();
                cmdB.Dispose();
                conn.Close();
            }
            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }



        [HttpPost]
        public IActionResult SubmitBidSubmittedDate()
        {

            String FinalStartDate = "";
            String BidSubDate = Request.Form["BidSubDate"];

            //HttpContext.Session.Remove("N");

            //HttpContext.Session.SetString("N", "Evaluation");
            Dblib dblib = new Dblib();
            if (BidSubDate != "")
            {
                FinalStartDate = dblib.getDate(BidSubDate);
            }

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            int TenderID = int.Parse(Request.Form["TenderID"]);



            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_Bid_Submitted_Date");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);


            cmdB.Parameters.AddWithValue("@varDate", FinalStartDate);



            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();


            cmdB.Dispose();
            conn.Close();
            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }



        [HttpPost]
        public IActionResult SubmitSynopsisStatus()
        {

            String Sstatus = Request.Form["TenderStatus"];

            HttpContext.Session.Remove("N");
            String Status = "";
            HttpContext.Session.SetString("N", "Synopsis");
          
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            int TenderID = int.Parse(Request.Form["TenderID"]);

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_By_ID");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);

            cmdB.Connection = conn;

           SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                Status = drB["Status"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            if (Sstatus == "Reject") {
                Status = "Rejected By Management";
            }


            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_Synopsis_Status");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
            cmdB.Parameters.AddWithValue("@varStatus", Status);


            cmdB.Parameters.AddWithValue("@varSStatus", Sstatus);



            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();


            cmdB.Dispose();
            conn.Close();
            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }



        [HttpPost]
        public IActionResult SubmitBECStatus()
        {

            String Sstatus = Request.Form["TenderStatus"];
            String Remark = Request.Form["remark"];
            String[] txt_note = Request.Form["txt_note[]"];

            int cnt = txt_note.Length;

           

            HttpContext.Session.Remove("N");
            String Status = "";
            HttpContext.Session.SetString("N", "BEC");

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            int TenderID = int.Parse(Request.Form["TenderID"]);

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_By_ID");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                Status = drB["Status"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            if (Sstatus == "Reject")
            {
                Status = "Rejected By Management";
            }


            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_BEC_Status");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
            cmdB.Parameters.AddWithValue("@varStatus", Status);
            cmdB.Parameters.AddWithValue("@varRemark", Remark);


            cmdB.Parameters.AddWithValue("@varSStatus", Sstatus);



            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();
            conn.Close();

            for (int i = 0; i < cnt; i++)
            {
                String V = txt_note[i];
                String[] words = txt_note[i].Split("_");
                if (words.Length == 2)
                {
                    String BECStatus = words[0];
                    Console.WriteLine(BECStatus);
                    int ID = int.Parse(words[1]);
                    Console.WriteLine(ID);


                    conn.Open();
                    cmdB = new SqlCommand();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Update_BEC_Status_Single");
                    cmdB.Parameters.AddWithValue("@varID", ID);
                    cmdB.Parameters.AddWithValue("@varStatus", BECStatus);




                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();


                    cmdB.Dispose();
                    conn.Close();
                    words = Array.Empty<String>();
                }
            }



            cmdB.Dispose();
            conn.Close();
            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }



        [HttpPost]
        public IActionResult SubmitSingleBECStatus()
        {

            String Sstatus = Request.Form["BECStatus"];


            HttpContext.Session.Remove("N");
            String Status = "";
            HttpContext.Session.SetString("N", "BEC");

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            int TenderID = int.Parse(Request.Form["TenderID"]);
            int BECID = int.Parse(Request.Form["BECSID"]);
            Console.WriteLine("B" + BECID);
            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_By_ID");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                Status = drB["Status"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

       


            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_BEC_Status_Single");
            cmdB.Parameters.AddWithValue("@varID", BECID);
            cmdB.Parameters.AddWithValue("@varStatus", Sstatus);




            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();


            cmdB.Dispose();
            conn.Close();
            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }




        [HttpPost]
        public IActionResult UpdateBECDetails()
        {
            List<Tender> Evaluation = new List<Tender>();


            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "BEC");
            int BID = int.Parse(Request.Form["BID"]);

            int TenderID = int.Parse(Request.Form["TenderID"]);


            String CriteriaCategory = Request.Form["CriteriaCategory"];
            String Other = "";
            if (CriteriaCategory == "Other") {

                Other = Request.Form["Other"];
            }
            String Criteria = Request.Form["Criteria"];


            String Satisfied = Request.Form["Satisfied"];

            String Remark = Request.Form["Remark"];

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            conn.Open();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_BEC");
            cmdB.Parameters.AddWithValue("@varID", BID);


            cmdB.Parameters.AddWithValue("@varCriteriaCategory", CriteriaCategory);
            cmdB.Parameters.AddWithValue("@varCriteria", Criteria);
            cmdB.Parameters.AddWithValue("@varOther", Other);

            cmdB.Parameters.AddWithValue("@varIsSatisfied", Satisfied);
            cmdB.Parameters.AddWithValue("@varRemark", Remark);


            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();


            cmdB.Dispose();
            conn.Close();
            ViewBag.Evaluation = Evaluation;
            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }



        [HttpPost]
        public async Task<IActionResult> UpdateSOR(Tender Plan)
        {
            List<Tender> Evaluation = new List<Tender>();
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            String SORCompetitor = "-";
            if (!String.IsNullOrEmpty(Request.Form["SORCompetitor"]))
            {
                SORCompetitor = Request.Form["SORCompetitor"];
            }
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "SOR");

            int TenderID = int.Parse(Request.Form["TenderID"]);
            int SORID = 0;

            String SORType = Request.Form["SORType"];

            String FileDetails = Request.Form["FileDetails"];


            String EstimatedCost = Request.Form["EstimatedCost"];



            conn.Open();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Get_Last_SOR_ID");




            cmdB.Connection = conn;
            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                SORID = int.Parse(drB["SORID"].ToString());


            }
            drB.Dispose();

            cmdB.Dispose();
            conn.Close();



            IFormFile file = Request.Form.Files.GetFile("doc");
            String FileName = "";
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            if (file != null)
            {



                FileName = userId + TenderID + SORID + file.FileName;

                CloudBlockBlob blob;
                String connStr = CDNSettings.Conn; ;

                CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer container = client.
                              GetContainerReference("pms");
                if (await container.ExistsAsync())
                {
                    FileName = userId + TenderID + SORID + file.FileName;
                    blob = container.GetBlockBlobReference(FileName);

                    if (await blob.ExistsAsync())
                    {
                        CloudBlockBlob snapshot = container.GetBlockBlobReference(FileName);
                        if (snapshot.SnapshotTime != null)
                        {
                            await blob.DeleteAsync();
                        }
                        await blob.DeleteAsync();


                    }
                }


                using (var memoryStream = file.OpenReadStream())
                {
                    //stream.CopyTo(memoryStream);
                    //imgByte = memoryStream.ToArray();



                    client = account.CreateCloudBlobClient();

                    container = client.
                    GetContainerReference("pms");

                    //container.CreateIfNotExists();

                    blob = container.
                        GetBlockBlobReference(FileName);

                    blob.Properties.ContentType = file.ContentType;

                    await blob.UploadFromStreamAsync(memoryStream);



                }

            }

            Dblib dblib = new Dblib();

            var SORLIST = new List<Tender>();
            try
            {
                using (var stream = new MemoryStream())
                {

                    await file.CopyToAsync(stream);
                    using (var package = new ExcelPackage(stream))
                    {

                        ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                        var rowcount = worksheet.Dimension.Rows;
                        for (int row = 2; row <= rowcount; row++)
                        {
                            try
                            {
                                SORLIST.Add(new Tender
                                {
                                    TenderRef = worksheet.Cells[row, 1].Value.ToString().Trim(),
                                    ClientName = worksheet.Cells[row, 2].Value.ToString().Trim(),

                                    HighLevScope = worksheet.Cells[row, 3].Value.ToString().Trim(),
                                    Sector = worksheet.Cells[row, 4].Value.ToString().Trim(),
                                    CostingGroup = worksheet.Cells[row, 5].Value.ToString().Trim(),
                                    CoverageORDeployment = worksheet.Cells[row, 6].Value.ToString().Trim(),
                                    CostEst = worksheet.Cells[row, 7].Value.ToString().Trim(),

                                });
                            }
                            catch (Exception e)
                            {

                                Console.WriteLine(e.ToString());

                            }

                        }
                    }

                }
            }
            catch (Exception e)
            {


                Message = "Error";
                Console.WriteLine(e.ToString());
            }


      

            for (int i = 0; i < SORLIST.Count; i++)
            {
                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_SOR");
                    cmdB.Parameters.AddWithValue("@varID", SORID);
                    cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                    cmdB.Parameters.AddWithValue("@varType", SORType);
                    cmdB.Parameters.AddWithValue("@varCompetitors", SORCompetitor);

                    cmdB.Parameters.AddWithValue("@varTenderRef", SORLIST[i].TenderRef);
                    cmdB.Parameters.AddWithValue("@varClientName", SORLIST[i].ClientName);
                    cmdB.Parameters.AddWithValue("@varHighLevScope", SORLIST[i].HighLevScope);
                    cmdB.Parameters.AddWithValue("@varSector", SORLIST[i].Sector);
                    cmdB.Parameters.AddWithValue("@varCostingGroup", SORLIST[i].CostingGroup);
                    cmdB.Parameters.AddWithValue("@varCoverageOR", SORLIST[i].CoverageORDeployment);
                    cmdB.Parameters.AddWithValue("@varCostEst", SORLIST[i].CostEst);
                    cmdB.Parameters.AddWithValue("@varUserID", userId);

                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();
             
                    cmdB.Dispose();
                }
                catch (Exception e)
                {


                    Console.WriteLine(e.Message);
                }
                finally
                {
                    conn.Close();


                }
            }


            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_SOR");
            cmdB.Parameters.AddWithValue("@varID", SORID);
            cmdB.Parameters.AddWithValue("@varContent", FileName );
            cmdB.Parameters.AddWithValue("@varStatus", "Draft");
            cmdB.Parameters.AddWithValue("@varAmount", EstimatedCost);
            cmdB.Parameters.AddWithValue("@varFileName", FileDetails);

            

            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();

            cmdB.Dispose();

            conn.Close();




            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }


        [HttpPost]
        public async Task<IActionResult> UpdateQuery(Tender Plan)
        {
            List<Tender> Evaluation = new List<Tender>();
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            String customerResponse = "-";
            if (!String.IsNullOrEmpty(Request.Form["customerResponse"]))
            {
                customerResponse = Request.Form["customerResponse"];
            }
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Queries");

            int TenderID = int.Parse(Request.Form["TenderID"]);
            int QID = int.Parse(Request.Form["QID"]);

            String FinalStartDate = "";

            String Date = Request.Form["Date"];
            Dblib dblib = new Dblib();
            if (Date != "")
            {
                FinalStartDate = dblib.getDate(Date);
            }


            String Name = "";

            IFormFile file = Request.Form.Files.GetFile("doc");
            String FileName = "";
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            if (file != null)
            {



                FileName = userId + TenderID + QID + file.FileName;

                CloudBlockBlob blob;
                String connStr = CDNSettings.Conn; ;

                CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer container = client.
                              GetContainerReference("pms");
                if (await container.ExistsAsync())
                {
                    FileName = userId + TenderID + QID + file.FileName;
                    blob = container.GetBlockBlobReference(FileName);

                    if (await blob.ExistsAsync())
                    {
                        CloudBlockBlob snapshot = container.GetBlockBlobReference(FileName);
                        if (snapshot.SnapshotTime != null)
                        {
                            await blob.DeleteAsync();
                        }
                        await blob.DeleteAsync();


                    }
                }


                using (var memoryStream = file.OpenReadStream())
                {
                    //stream.CopyTo(memoryStream);
                    //imgByte = memoryStream.ToArray();



                    client = account.CreateCloudBlobClient();

                    container = client.
                    GetContainerReference("pms");

                    //container.CreateIfNotExists();

                    blob = container.
                        GetBlockBlobReference(FileName);

                    blob.Properties.ContentType = file.ContentType;
                    Name = file.FileName;
                    await blob.UploadFromStreamAsync(memoryStream);



                }

            }







            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_Query");
            cmdB.Parameters.AddWithValue("@varID", QID);
            cmdB.Parameters.AddWithValue("@varContent", FileName);
            cmdB.Parameters.AddWithValue("@varStatus", "Closed");
            cmdB.Parameters.AddWithValue("@varCustomerResponse", customerResponse);
            cmdB.Parameters.AddWithValue("@varFileName", Name);
            cmdB.Parameters.AddWithValue("@varDate", FinalStartDate);



            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();

            cmdB.Dispose();

            conn.Close();




            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }

        public IActionResult AddQuery(int ID) {
            ViewBag.TenderID = ID;
            return View();
        }


        public IActionResult AddPQuery(int ID)
        {
            ViewBag.TenderID = ID;
            return View();
        }


        [HttpPost]
        public IActionResult AddQuery(Tender Plan)
        {
            List<Tender> Queries = new List<Tender>();
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
     
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Queries");

            int TenderID = int.Parse(Request.Form["TenderID"]);


            String TenderRef = "-";
            if (!String.IsNullOrEmpty(Request.Form["TenderRef"]))
            {
                TenderRef = Request.Form["TenderRef"];
            }
            String Article = "";
            if (!String.IsNullOrEmpty(Request.Form["Article"]))
            {
                Article = Request.Form["Article"];
            }

            String ArticleNo = "";
            if (!String.IsNullOrEmpty(Request.Form["ArticleNo"]))
            {
                ArticleNo = Request.Form["ArticleNo"];
            }

            String TenderCondition = "";
            if (!String.IsNullOrEmpty(Request.Form["TenderCondition"]))
            {
                TenderCondition = Request.Form["TenderCondition"];
            }

            String VCS = "";
            if (!String.IsNullOrEmpty(Request.Form["VCSQ"]))
            {
                VCS = Request.Form["VCSQ"];
            }

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId



            Dblib dblib = new Dblib();


            String FinalStartDate = "";

            String Date = Request.Form["Date"];
            if (Date != "")
            {
                FinalStartDate = dblib.getDate(Date);
            }

            String RoleName = "";
            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Users_By_ID");
            cmdB.Parameters.AddWithValue("@varID", userId);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                ViewBag.Id = int.Parse(drB["Id"].ToString());
                ViewBag.Email = drB["Email"].ToString();
                ViewBag.PhoneNumber = drB["PhoneNumber"].ToString();
                ViewBag.FirstName = drB["FirstName"].ToString();
                ViewBag.LastName = drB["LastName"].ToString();
                RoleName = drB["RoleName"].ToString();
                ViewBag.Department = drB["Department"].ToString();
                ViewBag.Type = drB["Type"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            String TenderNo = "";
            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_By_ID");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                TenderNo = drB["TenderNo"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Query");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varTenderNo", TenderNo);

                cmdB.Parameters.AddWithValue("@varRefDoc", TenderRef);
                cmdB.Parameters.AddWithValue("@varArticleNo", ArticleNo);
                cmdB.Parameters.AddWithValue("@varArticle", Article);
                cmdB.Parameters.AddWithValue("@varTenderCondition", TenderCondition);
                cmdB.Parameters.AddWithValue("@varVCSQuery", VCS);
                cmdB.Parameters.AddWithValue("@varUserID", userId);
                cmdB.Parameters.AddWithValue("@varDesignation", RoleName);
                cmdB.Parameters.AddWithValue("@varDate", FinalStartDate);


                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();

                cmdB.Dispose();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally {
                conn.Close();

            }







            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }


        [HttpPost]
        public IActionResult AddPQuery(Tender Plan)
        {
            List<Tender> Queries = new List<Tender>();
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Post Bid Queries");

            int TenderID = int.Parse(Request.Form["TenderID"]);



            String TenderRef = "-";
            if (!String.IsNullOrEmpty(Request.Form["TenderRef"]))
            {
                TenderRef = Request.Form["TenderRef"];
            }
            String Article = "";
            if (!String.IsNullOrEmpty(Request.Form["Article"]))
            {
                Article = Request.Form["Article"];
            }

            String ArticleNo = "";
            if (!String.IsNullOrEmpty(Request.Form["ArticleNo"]))
            {
                ArticleNo = Request.Form["ArticleNo"];
            }

            String TenderCondition = "";
            if (!String.IsNullOrEmpty(Request.Form["TenderCondition"]))
            {
                TenderCondition = Request.Form["TenderCondition"];
            }

            String VCS = "";
            if (!String.IsNullOrEmpty(Request.Form["VCSQ"]))
            {
                VCS = Request.Form["VCSQ"];
            }

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId



            Dblib dblib = new Dblib();


            String FinalStartDate = "";

            String Date = Request.Form["Date"];
            if (Date != "")
            {
                FinalStartDate = dblib.getDate(Date);
            }

            String RoleName = "";
            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Users_By_ID");
            cmdB.Parameters.AddWithValue("@varID", userId);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                ViewBag.Id = int.Parse(drB["Id"].ToString());
                ViewBag.Email = drB["Email"].ToString();
                ViewBag.PhoneNumber = drB["PhoneNumber"].ToString();
                ViewBag.FirstName = drB["FirstName"].ToString();
                ViewBag.LastName = drB["LastName"].ToString();
                RoleName = drB["RoleName"].ToString();
                ViewBag.Department = drB["Department"].ToString();
                ViewBag.Type = drB["Type"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            String TenderNo = "";
            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Tender_By_ID");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                TenderNo = drB["TenderNo"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_PQuery");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varTenderNo", TenderNo);

                cmdB.Parameters.AddWithValue("@varRefDoc", TenderRef);
                cmdB.Parameters.AddWithValue("@varArticleNo", ArticleNo);
                cmdB.Parameters.AddWithValue("@varArticle", Article);
                cmdB.Parameters.AddWithValue("@varTenderCondition", TenderCondition);
                cmdB.Parameters.AddWithValue("@varVCSQuery", VCS);
                cmdB.Parameters.AddWithValue("@varUserID", userId);
                cmdB.Parameters.AddWithValue("@varDesignation", RoleName);
                cmdB.Parameters.AddWithValue("@varDate", FinalStartDate);


                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();

                cmdB.Dispose();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                conn.Close();

            }



            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }



        [HttpPost]
        public async Task<IActionResult> UpdateManagementSOR(Tender Plan)
        {
            List<Tender> Evaluation = new List<Tender>();
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            String TenderStatus = "";
            String SORCompetitor = "-";
            if (!String.IsNullOrEmpty(Request.Form["SORCompetitor"]))
            {
                SORCompetitor = Request.Form["SORCompetitor"];
            }

        
            HttpContext.Session.Remove("N");
            String SORType = Request.Form["MSORType"];

            if (SORType == "Reject") {

                TenderStatus = "Rejected By Management";
            }

            HttpContext.Session.SetString("N", "SOR");

            int TenderID = int.Parse(Request.Form["TenderID"]);
            int SORID = 0;
            Console.WriteLine("S" + Request.Form["MSorID"]);
            SORID = 1;
            if (!String.IsNullOrEmpty(Request.Form["MSorID"]))
            {
                SORID = int.Parse(Request.Form["MSorID"]);
            }

            String FileDetails = "";
            if (!String.IsNullOrEmpty(Request.Form["FileDetails"]))
            {
                FileDetails = Request.Form["FileDetails"];
            }



            String EstimatedCost = "0";
            if (!String.IsNullOrEmpty(Request.Form["EstimatedCost"]))
            {
                EstimatedCost = Request.Form["EstimatedCost"];

            }


            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_SOR_Status");
            cmdB.Parameters.AddWithValue("@varID", SORID);
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);

            cmdB.Parameters.AddWithValue("@varStatus", SORType);


            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();

            cmdB.Dispose();

            conn.Close();

            if (SORType == "Reject") {
                conn.Open();
                cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Update_Tender_Status");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);


                cmdB.Parameters.AddWithValue("@varStatus", TenderStatus);



                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();
                conn.Close();

            }

            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Get_Last_MSOR_ID");




            cmdB.Connection = conn;
            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                SORID = int.Parse(drB["SORID"].ToString());


            }
            drB.Dispose();

            cmdB.Dispose();
            conn.Close();



            IFormFile file = Request.Form.Files.GetFile("doc");
            String FileName = "";
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            if (file != null)
            {



                FileName = userId + TenderID + SORID + file.FileName;

                CloudBlockBlob blob;
                String connStr = CDNSettings.Conn; ;

                CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer container = client.
                              GetContainerReference("pms");
                if (await container.ExistsAsync())
                {
                    FileName = userId + TenderID + SORID + file.FileName;
                    blob = container.GetBlockBlobReference(FileName);

                    if (await blob.ExistsAsync())
                    {
                        CloudBlockBlob snapshot = container.GetBlockBlobReference(FileName);
                        if (snapshot.SnapshotTime != null)
                        {
                            await blob.DeleteAsync();
                        }
                        await blob.DeleteAsync();


                    }
                }


                using (var memoryStream = file.OpenReadStream())
                {
                    //stream.CopyTo(memoryStream);
                    //imgByte = memoryStream.ToArray();



                    client = account.CreateCloudBlobClient();

                    container = client.
                    GetContainerReference("pms");

                    //container.CreateIfNotExists();

                    blob = container.
                        GetBlockBlobReference(FileName);

                    blob.Properties.ContentType = file.ContentType;

                    await blob.UploadFromStreamAsync(memoryStream);



                }

            }

            Dblib dblib = new Dblib();

            var SORLIST = new List<Tender>();
            try
            {
                using (var stream = new MemoryStream())
                {

                    await file.CopyToAsync(stream);
                    using (var package = new ExcelPackage(stream))
                    {

                        ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                        var rowcount = worksheet.Dimension.Rows;
                        for (int row = 2; row <= rowcount; row++)
                        {
                            try
                            {
                                SORLIST.Add(new Tender
                                {
                                    TenderRef = worksheet.Cells[row, 1].Value.ToString().Trim(),
                                    ClientName = worksheet.Cells[row, 2].Value.ToString().Trim(),

                                    HighLevScope = worksheet.Cells[row, 3].Value.ToString().Trim(),
                                    Sector = worksheet.Cells[row, 4].Value.ToString().Trim(),
                                    CostingGroup = worksheet.Cells[row, 5].Value.ToString().Trim(),
                                    CoverageORDeployment = worksheet.Cells[row, 6].Value.ToString().Trim(),
                                    CostEst = worksheet.Cells[row, 7].Value.ToString().Trim(),

                                });
                            }
                            catch (Exception e)
                            {

                                Console.WriteLine(e.ToString());

                            }

                        }
                    }

                }
            }
            catch (Exception e)
            {


                Message = "Error";
                Console.WriteLine(e.ToString());
            }




            for (int i = 0; i < SORLIST.Count; i++)
            {
                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_Management_SOR");
                    cmdB.Parameters.AddWithValue("@varID", SORID);
                    cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                    cmdB.Parameters.AddWithValue("@varType", "Self");
                    cmdB.Parameters.AddWithValue("@varCompetitors", "-");

                    cmdB.Parameters.AddWithValue("@varTenderRef", SORLIST[i].TenderRef);
                    cmdB.Parameters.AddWithValue("@varClientName", SORLIST[i].ClientName);
                    cmdB.Parameters.AddWithValue("@varHighLevScope", SORLIST[i].HighLevScope);
                    cmdB.Parameters.AddWithValue("@varSector", SORLIST[i].Sector);
                    cmdB.Parameters.AddWithValue("@varCostingGroup", SORLIST[i].CostingGroup);
                    cmdB.Parameters.AddWithValue("@varCoverageOR", SORLIST[i].CoverageORDeployment);
                    cmdB.Parameters.AddWithValue("@varCostEst", SORLIST[i].CostEst);
                    cmdB.Parameters.AddWithValue("@varUserID", userId);

                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();

                    cmdB.Dispose();
                }
                catch (Exception e)
                {


                    Console.WriteLine(e.Message);
                }
                finally
                {
                    conn.Close();


                }
            }


            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_SOR");
            cmdB.Parameters.AddWithValue("@varID", SORID);
            cmdB.Parameters.AddWithValue("@varContent", FileName);
            cmdB.Parameters.AddWithValue("@varStatus", "Draft");
            cmdB.Parameters.AddWithValue("@varAmount", EstimatedCost);
            cmdB.Parameters.AddWithValue("@varFileName", FileDetails);



            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();

            cmdB.Dispose();

            conn.Close();




            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }



        [HttpPost]
        public async Task<IActionResult> EditManagementSOR(Tender Plan)
        {
            List<Tender> Evaluation = new List<Tender>();
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            String SORCompetitor = "-";
            if (!String.IsNullOrEmpty(Request.Form["SORCompetitor"]))
            {
                SORCompetitor = Request.Form["SORCompetitor"];
            }


            HttpContext.Session.Remove("N");
            String SORType = Request.Form["MSORType"];

          

            HttpContext.Session.SetString("N", "SOR");

            int TenderID = int.Parse(Request.Form["TenderID"]);
            int SORID = 0;
            Console.WriteLine("S" + Request.Form["MSorID"]);
            SORID = 1;
            if (!String.IsNullOrEmpty(Request.Form["MSorID"]))
            {
                SORID = int.Parse(Request.Form["MSorID"]);
            }

            String FileDetails = "";
            if (!String.IsNullOrEmpty(Request.Form["FileDetails"]))
            {
                FileDetails = Request.Form["FileDetails"];
            }



            String EstimatedCost = "0";
            if (!String.IsNullOrEmpty(Request.Form["EstimatedCost"]))
            {
                EstimatedCost = Request.Form["EstimatedCost"];

            }



            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_Management_SOR");
            cmdB.Parameters.AddWithValue("@varTenderID", TenderID);



            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();

            cmdB.Dispose();

            conn.Close();

       
            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Get_Last_MSOR_ID");




            cmdB.Connection = conn;
            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                SORID = int.Parse(drB["SORID"].ToString());


            }
            drB.Dispose();

            cmdB.Dispose();
            conn.Close();



            IFormFile file = Request.Form.Files.GetFile("doc");
            String FileName = "";
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            if (file != null)
            {



                FileName = userId + TenderID + SORID + file.FileName;

                CloudBlockBlob blob;
                String connStr = CDNSettings.Conn; ;

                CloudStorageAccount account = CloudStorageAccount.Parse(connStr);

                CloudBlobClient client = account.CreateCloudBlobClient();
                CloudBlobContainer container = client.
                              GetContainerReference("pms");
                if (await container.ExistsAsync())
                {
                    FileName = userId + TenderID + SORID + file.FileName;
                    blob = container.GetBlockBlobReference(FileName);

                    if (await blob.ExistsAsync())
                    {
                        CloudBlockBlob snapshot = container.GetBlockBlobReference(FileName);
                        if (snapshot.SnapshotTime != null)
                        {
                            await blob.DeleteAsync();
                        }
                        await blob.DeleteAsync();


                    }
                }


                using (var memoryStream = file.OpenReadStream())
                {
                    //stream.CopyTo(memoryStream);
                    //imgByte = memoryStream.ToArray();



                    client = account.CreateCloudBlobClient();

                    container = client.
                    GetContainerReference("pms");

                    //container.CreateIfNotExists();

                    blob = container.
                        GetBlockBlobReference(FileName);

                    blob.Properties.ContentType = file.ContentType;

                    await blob.UploadFromStreamAsync(memoryStream);



                }

            }

            Dblib dblib = new Dblib();

            var SORLIST = new List<Tender>();
            try
            {
                using (var stream = new MemoryStream())
                {

                    await file.CopyToAsync(stream);
                    using (var package = new ExcelPackage(stream))
                    {

                        ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                        var rowcount = worksheet.Dimension.Rows;
                        for (int row = 2; row <= rowcount; row++)
                        {
                            try
                            {
                                SORLIST.Add(new Tender
                                {
                                    TenderRef = worksheet.Cells[row, 1].Value.ToString().Trim(),
                                    ClientName = worksheet.Cells[row, 2].Value.ToString().Trim(),

                                    HighLevScope = worksheet.Cells[row, 3].Value.ToString().Trim(),
                                    Sector = worksheet.Cells[row, 4].Value.ToString().Trim(),
                                    CostingGroup = worksheet.Cells[row, 5].Value.ToString().Trim(),
                                    CoverageORDeployment = worksheet.Cells[row, 6].Value.ToString().Trim(),
                                    CostEst = worksheet.Cells[row, 7].Value.ToString().Trim(),

                                });
                            }
                            catch (Exception e)
                            {

                                Console.WriteLine(e.ToString());

                            }

                        }
                    }

                }
            }
            catch (Exception e)
            {


                Message = "Error";
                Console.WriteLine(e.ToString());
            }




            for (int i = 0; i < SORLIST.Count; i++)
            {
                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_Management_SOR");
                    cmdB.Parameters.AddWithValue("@varID", SORID);
                    cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                    cmdB.Parameters.AddWithValue("@varType", "Self");
                    cmdB.Parameters.AddWithValue("@varCompetitors", "-");

                    cmdB.Parameters.AddWithValue("@varTenderRef", SORLIST[i].TenderRef);
                    cmdB.Parameters.AddWithValue("@varClientName", SORLIST[i].ClientName);
                    cmdB.Parameters.AddWithValue("@varHighLevScope", SORLIST[i].HighLevScope);
                    cmdB.Parameters.AddWithValue("@varSector", SORLIST[i].Sector);
                    cmdB.Parameters.AddWithValue("@varCostingGroup", SORLIST[i].CostingGroup);
                    cmdB.Parameters.AddWithValue("@varCoverageOR", SORLIST[i].CoverageORDeployment);
                    cmdB.Parameters.AddWithValue("@varCostEst", SORLIST[i].CostEst);
                    cmdB.Parameters.AddWithValue("@varUserID", userId);

                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();

                    cmdB.Dispose();
                }
                catch (Exception e)
                {


                    Console.WriteLine(e.Message);
                }
                finally
                {
                    conn.Close();


                }
            }




            return RedirectToAction("TenderDetails", new { ID = TenderID });

        }


        public IActionResult RemoveAccess(int ID, int T)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "User Access");

            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Remove_Access_From_Tender");
            cmdB.Parameters.AddWithValue("@varID", ID);



            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();

            cmdB.Dispose();

            conn.Close();

            return RedirectToAction("TenderDetails", new { ID = T });

        }

        public IActionResult SORApprove(int ID, int T) {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "SOR");

            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_SOR_Status");
            cmdB.Parameters.AddWithValue("@varID", ID);
            cmdB.Parameters.AddWithValue("@varTenderID", T);

            cmdB.Parameters.AddWithValue("@varStatus", "Under Review");


            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();

            cmdB.Dispose();

            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Checklist_Users_By_Tender_ID_Manager");
            cmdB.Parameters.AddWithValue("@varID", T);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                try
                {
                    cmdB = new SqlCommand();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_Notifications");
                    cmdB.Parameters.AddWithValue("@varStatus", "Pending");
                    cmdB.Parameters.AddWithValue("@varType", "Manager");
                    cmdB.Parameters.AddWithValue("@varNote", "SOR For Tender Needs Your Approval");
                    cmdB.Parameters.AddWithValue("@varUserID", int.Parse(drB["UserId"].ToString()));
                    cmdB.Parameters.AddWithValue("@varLink", "TenderDetails/" + T + "");
                    cmdB.Parameters.AddWithValue("@varCreatedBy", userId);




                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();
                    cmdB.Dispose();

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {

                }


            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            return RedirectToAction("TenderDetails", new { ID = T });

        }


        public IActionResult BECApprove(int ID)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "BEC");

            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_BEC_Status_Pending");
            cmdB.Parameters.AddWithValue("@varID", ID);
            cmdB.Parameters.AddWithValue("@varStatus", "Pending Approval");


            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();

            cmdB.Dispose();

            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Checklist_Users_By_Tender_ID_Manager");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                try
                {
                    cmdB = new SqlCommand();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_Notifications");
                    cmdB.Parameters.AddWithValue("@varStatus", "Pending");
                    cmdB.Parameters.AddWithValue("@varType", "Manager");
                    cmdB.Parameters.AddWithValue("@varNote", "BEC For Tender Needs Your Approval");
                    cmdB.Parameters.AddWithValue("@varUserID", int.Parse(drB["UserId"].ToString()));
                    cmdB.Parameters.AddWithValue("@varLink", "TenderDetails/" + ID + "");
                    cmdB.Parameters.AddWithValue("@varCreatedBy", userId);




                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();
                    cmdB.Dispose();

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally { 
                
                }


            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();





            return RedirectToAction("TenderDetails", new { ID = ID });

        }


        public IActionResult SynopsisApprove(int ID)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Synopsis");

            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Update_Synopsis_Status_Pending");
            cmdB.Parameters.AddWithValue("@varID", ID);
            cmdB.Parameters.AddWithValue("@varStatus", "Pending Approval");


            cmdB.Connection = conn;
            cmdB.ExecuteNonQuery();

            cmdB.Dispose();

            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Checklist_Users_By_Tender_ID_Manager");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                try
                {
                    cmdB = new SqlCommand();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_Notifications");
                    cmdB.Parameters.AddWithValue("@varStatus", "Pending");
                    cmdB.Parameters.AddWithValue("@varType", "Manager");
                    cmdB.Parameters.AddWithValue("@varNote", "Synopsis For Tender Needs Your Approval");
                    cmdB.Parameters.AddWithValue("@varUserID", int.Parse(drB["UserId"].ToString()));
                    cmdB.Parameters.AddWithValue("@varLink", "TenderDetails/" + ID + "");
                    cmdB.Parameters.AddWithValue("@varCreatedBy", userId);




                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();
                    cmdB.Dispose();

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {

                }


            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            return RedirectToAction("TenderDetails", new { ID = ID });

        }



        public IActionResult DeleteTender(int? ID)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_Tender");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("Tenders");
        }

        public IActionResult DeleteSWOT(int? ID)
        {

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "SWOT");
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_SWOT");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("Tenders");
        }

        public IActionResult DeleteQuery(int? ID)
        {

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Queries");
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_Query");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("Tenders");
        }



        public IActionResult DeleteOtherInfo(int? ID)
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Other Info");
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_OtherInfo");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("TenderDetails", new { ID = ID});
        }

        public IActionResult DeletePostAward(int? ID)
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Post Award Analysis");
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_Post_Award");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("TenderDetails", new { ID = ID });
        }





        public IActionResult DeleteDepartment(int? ID)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_Role");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("Roles");
        }

        public IActionResult DeleteUser(int? ID)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_User");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("Users");
        }




        public IActionResult DeleteEvaluation(int? ID)
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Evaluation");
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_Evaluation");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("Tenders");
        }

        public IActionResult DeleteChecklist(int? ID)
        {
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Checklist");
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_Checklist");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("Tenders");
        }




        public IActionResult DeleteBEC(int? ID)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Add_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_BEC");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "BEC");

            return RedirectToAction("Tenders");
        }




        [HttpPost]
        public IActionResult AddTenderDetails()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            Dblib dblib = new Dblib();





            String FinalTenderNFDate = "";
            String FinalBidSDateOn = "";
            String FinalBidSDateOff = "";
            String FinalBidOpeningDate = "";
            String FinalPreBidDate = "";


            int TenderID = 0;

            String ClientName = Request.Form["ClientName"];

            String TenderNo = Request.Form["TenderNo"];
            String TenderName = Request.Form["TenderName"];
            String ScopeOfWork = Request.Form["ScopeOfWork"];
            String BidValidity = Request.Form["BidValidity"];

            int Category = int.Parse(Request.Form["TenderCategory"]);

            //String LocationInfo = Request.Form["LocationInfo"];

            //String Location = Request.Form["LocationName"];
            String Region = Request.Form["Region"];
            String Source = Request.Form["Source"];
            String SourceDetails = Request.Form["SourceDetails"];

            String TenderNFDate = Request.Form["TenderNFDate"];
            String BidSDateOn = Request.Form["BidSDateOn"];
            String BidSDateOff = Request.Form["BidSDateOff"];

            String BidSTimeOn = Request.Form["BidSTimeOn"];
            String BidSTimeOff = Request.Form["BidSTimeOff"];

            String PreBidDate = Request.Form["PreBidDate"];
            String PreBidTime = Request.Form["PreBidTime"];
            String ContactDetails = Request.Form["ContactDetails"];
            String Remark = Request.Form["Remark"];
            String Designation = Request.Form["Designation"];
            String Email = Request.Form["Email"];

            String PhoneNumber = Request.Form["PhoneNumber"];

            String BidOpeningDate = Request.Form["BidOpeningDate"];

            String BidOpeningTime = Request.Form["BidOpeningTime"];

            String EMD = Request.Form["EMD"];

            String EMDE = Request.Form["EMDE"];

            String TenderFeeDetails = Request.Form["TenderFeeDetails"];

            String TEx = Request.Form["TEx"];

            String ContractValue = Request.Form["ContractValue"];

            String ContractPeriod = Request.Form["ContractPeriod"];

            String ExContractPeriod = Request.Form["ExContractPeriod"];

            int Priority = int.Parse(Request.Form["Priority"]);

            int Industry = int.Parse(Request.Form["Industry"]);

            int TenderCategory = int.Parse(Request.Form["TenderCategory"]);

            int TenderType = int.Parse(Request.Form["TenderType"]);

            int BiddingType = int.Parse(Request.Form["BiddingType"]);

            String Mode = Request.Form["Mode"];


            String JV = Request.Form["JV"];

            String BidAward = Request.Form["BidAward"];


            Double TenderFee = Double.Parse(Request.Form["TenderFee"]);


            if (PreBidDate !=null) {

                FinalPreBidDate = dblib.getDate(PreBidDate);
            }
            if (TenderNFDate != "")
            {
                FinalTenderNFDate = dblib.getDate(TenderNFDate);
            }


            if (BidOpeningDate != "")
            {
                FinalBidOpeningDate = dblib.getDate(BidOpeningDate);
            }


            if (BidSDateOn != "")
            {
                FinalBidSDateOn = dblib.getDate(BidSDateOn);
            }


            if (BidSDateOff != "")
            {
                FinalBidSDateOff = dblib.getDate(BidSDateOff);
            }


            try
            {
                conn.Open();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Select_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Select_Latest_Tender");
                cmdB.Connection = conn;
                SqlDataReader drB = cmdB.ExecuteReader();
                while (drB.Read())
                {
                    TenderID = int.Parse(drB["TenderID"].ToString());

                }

                cmdB.Dispose();
                drB.Dispose();

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                
                conn.Close();
            }
            Console.WriteLine(TenderID);

            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Tender");
                cmdB.Parameters.AddWithValue("@varTenderNo", TenderNo);
                cmdB.Parameters.AddWithValue("@varClientName", ClientName);
                cmdB.Parameters.AddWithValue("@varTenderName", TenderName);
                cmdB.Parameters.AddWithValue("@varBidValidity", BidValidity);

                cmdB.Parameters.AddWithValue("@varScope", ScopeOfWork);

                cmdB.Parameters.AddWithValue("@varCategory", Category);
                cmdB.Parameters.AddWithValue("@varRegion", Region);
                cmdB.Parameters.AddWithValue("@varSource", Source);
                cmdB.Parameters.AddWithValue("@varTenderFee", TenderFee);

                cmdB.Parameters.AddWithValue("@varSourceDetails", SourceDetails);
                cmdB.Parameters.AddWithValue("@varTenderNFDate", FinalTenderNFDate);
                cmdB.Parameters.AddWithValue("@varBillSubDateOnline", FinalBidSDateOn);
                cmdB.Parameters.AddWithValue("@varBillSubDateOffline", FinalBidSDateOff);
                cmdB.Parameters.AddWithValue("@varStatus", "Prospective");
                cmdB.Parameters.AddWithValue("@varContactDetails", ContactDetails);
                cmdB.Parameters.AddWithValue("@varRemark", Remark);
                cmdB.Parameters.AddWithValue("@varTenderType", TenderType);
                cmdB.Parameters.AddWithValue("@varPreBidDate", FinalPreBidDate);
                cmdB.Parameters.AddWithValue("@varPreBidTime", PreBidTime);
                cmdB.Parameters.AddWithValue("@varBidTimeOl", BidSTimeOn);
                cmdB.Parameters.AddWithValue("@varBidTimeOf", BidSTimeOff);

                cmdB.Parameters.AddWithValue("@varBidOpeningTime", BidOpeningTime);
                cmdB.Parameters.AddWithValue("@varBidOpeningDate", FinalBidOpeningDate);
                cmdB.Parameters.AddWithValue("@varDesignation", Designation);
                cmdB.Parameters.AddWithValue("@varEmail", Email);
                cmdB.Parameters.AddWithValue("@varPhoneNumber", PhoneNumber);
                cmdB.Parameters.AddWithValue("@varEMD", EMD);
                cmdB.Parameters.AddWithValue("@varEMDFlag", EMDE);
                cmdB.Parameters.AddWithValue("@varTenderFeeDetails", TenderFeeDetails);
                cmdB.Parameters.AddWithValue("@varTenderFeeExempted", TEx);
                cmdB.Parameters.AddWithValue("@varContractValue", ContractValue);
                cmdB.Parameters.AddWithValue("@varPriority", Priority);
                cmdB.Parameters.AddWithValue("@varContractPeriod", ContractPeriod);
                cmdB.Parameters.AddWithValue("@varExContractPeriod", ExContractPeriod);
                cmdB.Parameters.AddWithValue("@varIndustry", Industry);
                cmdB.Parameters.AddWithValue("@varTenderCategory", TenderCategory);
                cmdB.Parameters.AddWithValue("@varBiddingType", BiddingType);
                cmdB.Parameters.AddWithValue("@varMode", Mode);
                cmdB.Parameters.AddWithValue("@varJV", JV);
                cmdB.Parameters.AddWithValue("@varBidAward", BidAward);


                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();
        


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }




            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Tender_User");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varID", userId);

                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);


                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();



            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }

            return RedirectToAction("TenderDetails", new { ID = TenderID });
        }



        [HttpPost]
        public IActionResult AddOtherTenderInfo()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            Dblib dblib = new Dblib();



            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Other Info");

            String ManpowerDetails = Request.Form["ManpowerDetails"];

            String Qualification = Request.Form["Qualification"];
            String Transportation = Request.Form["Transportation"];
            String Equipment = Request.Form["Equipment"];
            String OfficeSetup = Request.Form["OfficeSetup"];

            int TenderID = int.Parse(Request.Form["TenderID"]);

            //String LocationInfo = Request.Form["LocationInfo"];

            //String Location = Request.Form["LocationName"];
            String GuestHouse = Request.Form["GuestHouse"];
            String MPeriod = Request.Form["MPeriod"];
            String MAdvance = Request.Form["MAdvance"];

            String Payment = Request.Form["Payment"];
            String Security = Request.Form["Security"];
            String Liability = Request.Form["Liability"];

            String Penalties = Request.Form["Penalties"];
            String Liquidity = Request.Form["Liquidity"];

            String Insurance = Request.Form["Insurance"];
            String Price = Request.Form["Price"];
            String Purchase = Request.Form["Purchase"];
            String Clause = Request.Form["Clause"];
            String Experience = Request.Form["Experience"];
            String Competitors = Request.Form["Competitors"];

            String Criticial = Request.Form["Criticial"];

            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_OtherInfo");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varManpowerDetails", ManpowerDetails);
                cmdB.Parameters.AddWithValue("@varManpowerQualification", Qualification);
                cmdB.Parameters.AddWithValue("@varEquipmentConsumables", Equipment);

                cmdB.Parameters.AddWithValue("@varTransportationRequirment", Transportation);

                cmdB.Parameters.AddWithValue("@varOfficeSetup", OfficeSetup);
                cmdB.Parameters.AddWithValue("@varGuestHouse", GuestHouse);
                cmdB.Parameters.AddWithValue("@varMobPeriod", MPeriod);
                cmdB.Parameters.AddWithValue("@varMobAdvance", MAdvance);

                cmdB.Parameters.AddWithValue("@varSecurity", Security);
                cmdB.Parameters.AddWithValue("@varDefaultPeriod", Liability);
                cmdB.Parameters.AddWithValue("@varPenalities", Penalties);
                cmdB.Parameters.AddWithValue("@varPaymentTerms", Payment);
                cmdB.Parameters.AddWithValue("@varLiquidityDamage", Liquidity);
                cmdB.Parameters.AddWithValue("@varInsurance", Insurance);
                cmdB.Parameters.AddWithValue("@varPrice", Price);
                cmdB.Parameters.AddWithValue("@varPurchase", Purchase);
                cmdB.Parameters.AddWithValue("@varStartUpClause", Clause);
                cmdB.Parameters.AddWithValue("@varParentCompany", Experience);
                cmdB.Parameters.AddWithValue("@varCompetitors", Competitors);
                cmdB.Parameters.AddWithValue("@varAnyOther", Criticial);

       
                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();



            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }
            return RedirectToAction("TenderDetails", new { ID = TenderID });
        }


        [HttpPost]
        public IActionResult UpdateOtherTenderInfo()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            Dblib dblib = new Dblib();



            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Other Info");


            String ManpowerDetails = Request.Form["ManpowerDetails"];

            String Qualification = Request.Form["Qualification"];
            String Transportation = Request.Form["Transportation"];
            String Equipment = Request.Form["Equipment"];
            String OfficeSetup = Request.Form["OfficeSetup"];

            int InfoID = int.Parse(Request.Form["InfoID"]);
            int TenderID = int.Parse(Request.Form["TenderID"]);

            //String LocationInfo = Request.Form["LocationInfo"];

            //String Location = Request.Form["LocationName"];
            String GuestHouse = Request.Form["GuestHouse"];
            String MPeriod = Request.Form["MPeriod"];
            String MAdvance = Request.Form["MAdvance"];

            String Payment = Request.Form["Payment"];
            String Security = Request.Form["Security"];
            String Liability = Request.Form["Liability"];

            String Penalties = Request.Form["Penalties"];
            String Liquidity = Request.Form["Liquidity"];

            String Insurance = Request.Form["Insurance"];
            String Price = Request.Form["Price"];
            String Purchase = Request.Form["Purchase"];
            String Clause = Request.Form["Clause"];
            String Experience = Request.Form["Experience"];
            String Competitors = Request.Form["Competitors"];

            String Criticial = Request.Form["Criticial"];

            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Update_OtherInfo");
                cmdB.Parameters.AddWithValue("@varID", InfoID);
                cmdB.Parameters.AddWithValue("@varManpowerDetails", ManpowerDetails);
                cmdB.Parameters.AddWithValue("@varManpowerQualification", Qualification);
                cmdB.Parameters.AddWithValue("@varEquipmentConsumables", Equipment);

                cmdB.Parameters.AddWithValue("@varTransportationRequirment", Transportation);

                cmdB.Parameters.AddWithValue("@varOfficeSetup", OfficeSetup);
                cmdB.Parameters.AddWithValue("@varGuestHouse", GuestHouse);
                cmdB.Parameters.AddWithValue("@varMobPeriod", MPeriod);
                cmdB.Parameters.AddWithValue("@varMobAdvance", MAdvance);

                cmdB.Parameters.AddWithValue("@varSecurity", Security);
                cmdB.Parameters.AddWithValue("@varDefaultPeriod", Liability);
                cmdB.Parameters.AddWithValue("@varPenalities", Penalties);
                cmdB.Parameters.AddWithValue("@varPaymentTerms", Payment);
                cmdB.Parameters.AddWithValue("@varLiquidityDamage", Liquidity);
                cmdB.Parameters.AddWithValue("@varInsurance", Insurance);
                cmdB.Parameters.AddWithValue("@varPrice", Price);
                cmdB.Parameters.AddWithValue("@varPurchase", Purchase);
                cmdB.Parameters.AddWithValue("@varStartUpClause", Clause);
                cmdB.Parameters.AddWithValue("@varParentCompany", Experience);
                cmdB.Parameters.AddWithValue("@varCompetitors", Competitors);
                cmdB.Parameters.AddWithValue("@varAnyOther", Criticial);


                cmdB.Parameters.AddWithValue("@varModifiedBy", userId);

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();



            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }
            return RedirectToAction("TenderDetails", new { ID = TenderID });
        }

        public IActionResult AddEmployee() {



            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
      

            List<ApplicationUser> Roles = new List<ApplicationUser>();

            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Roles");

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Roles.Add(new ApplicationUser
                {
                    Id = int.Parse(drB["Id"].ToString()),

                    FirstName = drB["Name"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.Roles = Roles;
            return View();
        }


        [HttpPost]
        public IActionResult AddToTender(Tender model) {

            int TenderID = int.Parse(Request.Form["TenderID"]);
            int ID = int.Parse(Request.Form["UserName"]);
            Console.WriteLine(TenderID);
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Tender_User");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varID", ID);

                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);


                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();



            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }


            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "User Access");

            return RedirectToAction("TenderDetails", new { ID = TenderID });
        }

        public async Task<IActionResult> RegisterEmployee(Employee model)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            String roleID = Request.Form["Role"];
            String TypeUser = "";
            if (!String.IsNullOrEmpty(Request.Form["Type"]))
            {
                TypeUser = Request.Form["Type"];
            }
            else {
                TypeUser = Request.Form["Role"];
                if (TypeUser == "Management") {
                    TypeUser = "Management";
                }

            }


            String[] txt_note = Request.Form["txt_note[]"];
            String[] txt_note2 = Request.Form["txt_note2[]"];
            int cnt = txt_note.Length;



        


            if (ModelState.IsValid)
            {
                // Copy data from RegisterViewModel to IdentityUser


                var user = new ApplicationUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    PhoneNumber = model.PhoneNumber,
                    FirstName = model.FirstName,
                    MiddleName = model.MiddleName,
                    LastName = model.LastName,
                    IsAccountActive = 1,
                    EmailConfirmed = true,
                    PhoneNumberConfirmed = true,
                    CreatedOn = DateTime.Now,
                    Department = model.Department
                };

                var PhoneNumber = _db.Users.Where(s => s.PhoneNumber == model.PhoneNumber);
                var Email = _db.Users.Where(s => s.Email == model.Email);

                if (PhoneNumber.Count() > 0 || Email.Count() > 0)
                {
                    ModelState.AddModelError(String.Empty, "Mobile Number is already registered!");
                    Message = "Failed";
                    return RedirectToAction("Employee");
                }
                // Store user data in AspNetUsers database table
                var result = await userManager.CreateAsync(user, model.PhoneNumber);




                // Saves the role in the underlying AspNetRoles table
                Console.WriteLine(roleID);
                result = await userManager.AddToRoleAsync(user, roleID);

                var LatestUserID = userManager.GetUserId(User); // Get user id:


                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_UserType");
                    cmdB.Parameters.AddWithValue("@varType", TypeUser);
                    cmdB.Parameters.AddWithValue("@varID", user.Id);



                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();



                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);

                }
                finally
                {
                    cmdB.Dispose();

                    conn.Close();

                }

                var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId


                for (int i = 0; i < cnt; i++)
                {
                    conn.Open();
                    cmdB = new SqlCommand();
                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_Module_Access");
                    cmdB.Parameters.AddWithValue("@varUserID", user.Id);
                    cmdB.Parameters.AddWithValue("@varModID", i+1);
                    cmdB.Parameters.AddWithValue("@varModuleName", txt_note[i]);
                    cmdB.Parameters.AddWithValue("@varType", txt_note2[i]);

                    cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();
                    cmdB.Dispose();
                    conn.Close();

                }





                // If user is successfully created, sign-in the user using
                // SignInManager and redirect to index action of HomeController
                if (result.Succeeded)
                {


                    Message = "Success";
                    return RedirectToAction("Users");

                }


                // If there are any errors, add them to the ModelState object
                // which will be displayed by the validation summary tag helper
                foreach (var error in result.Errors)
                {
                    if (error.Code.Contains("DuplicateUserName"))
                    {
                        ModelState.AddModelError(String.Empty, "Email is already registered!");
                        break;
                    }
                    ModelState.AddModelError(String.Empty, error.Description);
                }
            }

            Message = "Error";

            return RedirectToAction("Users");


        }

        [HttpPost]
        public async Task<IActionResult> UpdateEmployee(Employee model)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            String roleID = Request.Form["Role"];
            String TypeUser = Request.Form["Type"];

            if (roleID == "Management")
            {
                TypeUser = "Management";
            }
            else if (roleID == "Admin") {
                TypeUser = "Admin";

            }

            String[] txt_note = Request.Form["txt_note[]"];
            String[] txt_note2 = Request.Form["txt_note2[]"];
            int employeeID = int.Parse(Request.Form["employeeID"]);

            int cnt = txt_note.Length;





            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            if (ModelState.IsValid)
            {
                // Copy data from RegisterViewModel to IdentityUser



                var user = await userManager.FindByEmailAsync(model.Email);


                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();
                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "DeleteUserRole");
                    cmdB.Parameters.AddWithValue("@varID", employeeID);

                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {
                    cmdB.Dispose();

                    conn.Close();

                }
                var roleUpdate = await userManager.AddToRoleAsync(user, roleID);


                user.UserName = model.Email;
                user.Email = model.Email;
                user.PhoneNumber = model.PhoneNumber;
                user.FirstName = model.FirstName;
                user.LastName = model.LastName;

                IdentityResult result = await userManager.UpdateAsync(user);



                Console.WriteLine("e"+employeeID);

                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();
                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Remove_Module_Access");
                    cmdB.Parameters.AddWithValue("@varID", employeeID);

                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally {
                    cmdB.Dispose();

                    conn.Close();

                }

                for (int i = 0; i < cnt; i++)
                {
                    conn.Open();
                    cmdB = new SqlCommand();
                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_Module_Access");
                    cmdB.Parameters.AddWithValue("@varUserID", employeeID);
                    cmdB.Parameters.AddWithValue("@varModID", i + 1);
                    cmdB.Parameters.AddWithValue("@varModuleName", txt_note[i]);
                    cmdB.Parameters.AddWithValue("@varType", txt_note2[i]);

                    cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();
                    cmdB.Dispose();
                    conn.Close();

                }


                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();
                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "UpdateUserType");
                    cmdB.Parameters.AddWithValue("@varID", employeeID);
                    cmdB.Parameters.AddWithValue("@varType", TypeUser);

                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
                finally
                {
                    cmdB.Dispose();

                    conn.Close();

                }




                // If user is successfully created, sign-in the user using
                // SignInManager and redirect to index action of HomeController
                if (result.Succeeded)
                {


                    Message = "Success";
                    return RedirectToAction("Users");

                }


        
                // If there are any errors, add them to the ModelState object
                // which will be displayed by the validation summary tag helper
                foreach (var error in result.Errors)
                {
                    if (error.Code.Contains("DuplicateUserName"))
                    {
                        ModelState.AddModelError(String.Empty, "Email is already registered!");
                        break;
                    }
                    ModelState.AddModelError(String.Empty, error.Description);
                }
            }

            Message = "Error";

            return RedirectToAction("Users");


        }




        public IActionResult RegisterRole(Employee model)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();
            Console.WriteLine(model.FirstName);
          
                // Copy data from RegisterViewModel to IdentityUser

                if (_db.Roles.Any(r => r.Name == model.FirstName))
                {
                    return RedirectToAction("Roles");
                };

                _ = this.roleManager.CreateAsync(new ApplicationRole(model.FirstName)).GetAwaiter().GetResult();
            


            return RedirectToAction("Roles");


        }



        public IActionResult Users() {
            List<ApplicationUser> User = new List<ApplicationUser>();



            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Users");

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                User.Add(new ApplicationUser
                {
                    Id = int.Parse(drB["Id"].ToString()),
                    Email = drB["Email"].ToString(),
                    PhoneNumber = drB["PhoneNumber"].ToString(),
                    FirstName = drB["FirstName"].ToString() +" "+ drB["LastName"].ToString(),
                    LastName = drB["RoleName"].ToString(),
                    Department = drB["Department"].ToString()


            });
            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();





            ViewBag.Users = User;

            return View();
        }



        public IActionResult ViewUser(int ID)
        {
            List<ApplicationUser> User = new List<ApplicationUser>();

            List<ApplicationUser> Roles = new List<ApplicationUser>();


            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Users_By_ID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                ViewBag.Id = int.Parse(drB["Id"].ToString());
                ViewBag.Email = drB["Email"].ToString();
                ViewBag.PhoneNumber = drB["PhoneNumber"].ToString();
                ViewBag.FirstName = drB["FirstName"].ToString();
                ViewBag.LastName = drB["LastName"].ToString();
                ViewBag.RoleName = drB["RoleName"].ToString();
                ViewBag.Department = drB["Department"].ToString();
                ViewBag.Type = drB["Type"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            List<Tender> ModuleList = new List<Tender>();

            try
            {
                conn.Open();

                cmdB = new SqlCommand();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Select_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Select_Admin_Access_By_ID");
                cmdB.Parameters.AddWithValue("@varID", ID);

                cmdB.Connection = conn;

                drB = cmdB.ExecuteReader();



                while (drB.Read())
                {
                    ModuleList.Add(new Tender
                    {
                        Permission = drB["Permission"].ToString(),

                        ModuleName = drB["ModuleName"].ToString(),

                    });
                }

                drB.Dispose();
                cmdB.Dispose();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
            finally
            {
                conn.Close();
            }

            ViewBag.ModuleList = ModuleList;

            ViewBag.Users = User;


            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Roles");

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Roles.Add(new ApplicationUser
                {
                    Id = int.Parse(drB["Id"].ToString()),

                    FirstName = drB["Name"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.Roles = Roles;
            Console.WriteLine(ViewBag.Roles.Count);

            return View();
        }


        public IActionResult ViewProfile()
        {


            List<ApplicationUser> Roles = new List<ApplicationUser>();

            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);// will give the user's userId


            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Users_By_ID");
            cmdB.Parameters.AddWithValue("@varID", userId);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                ViewBag.Id = int.Parse(drB["Id"].ToString());
                ViewBag.Email = drB["Email"].ToString();
                ViewBag.PhoneNumber = drB["PhoneNumber"].ToString();
                ViewBag.FirstName = drB["FirstName"].ToString();
                ViewBag.LastName = drB["LastName"].ToString();
                ViewBag.RoleName = drB["RoleName"].ToString();
                ViewBag.Department = drB["Department"].ToString();
                ViewBag.Type = drB["Type"].ToString();

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();


            List<Tender> ModuleList = new List<Tender>();

            try
            {
                conn.Open();

                cmdB = new SqlCommand();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Select_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Select_Admin_Access_By_ID");
                cmdB.Parameters.AddWithValue("@varID", userId);

                cmdB.Connection = conn;

                drB = cmdB.ExecuteReader();



                while (drB.Read())
                {
                    ModuleList.Add(new Tender
                    {
                        Permission = drB["Permission"].ToString(),

                        ModuleName = drB["ModuleName"].ToString(),

                    });
                }

                drB.Dispose();
                cmdB.Dispose();
            }
            catch (Exception e)
            {

                Console.WriteLine(e.ToString());
            }
            finally
            {
                conn.Close();
            }

            ViewBag.ModuleList = ModuleList;



            conn.Open();
            cmdB = new SqlCommand();

            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Roles");

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Roles.Add(new ApplicationUser
                {
                    Id = int.Parse(drB["Id"].ToString()),

                    FirstName = drB["Name"].ToString(),

                });
            }


            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            ViewBag.Roles = Roles;
            Console.WriteLine(ViewBag.Roles.Count);

            return View();
        }


        [HttpPost]
        public async Task<IActionResult> UpdateRoleAsync()
        {
            var roleID = Request.Form["RoleID"];
            String roleName = Request.Form["RoleName"];
            try
            {
                ApplicationRole role = await roleManager.FindByIdAsync(roleID);
                role.Name = role.Name;
                await roleManager.UpdateAsync(role);
                return RedirectToAction("Roles");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return View();
            }
        }

        public IActionResult ViewRole(int ID)
        {

            List<ApplicationUser> Roles = new List<ApplicationUser>();
            ViewBag.ID = ID;
            String DeleteFlag = "";
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Roles_By_ID");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
     
                ViewBag.Name = drB["Name"].ToString();
                ViewBag.RoleID = int.Parse(drB["Id"].ToString());

            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_PMS_Select_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Role_Exist");
            cmdB.Parameters.AddWithValue("@varID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {

                ViewBag.DeleteFlag = drB["RoleId"].ToString();


            }

            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            ViewBag.DeleteFlag = DeleteFlag;

            Console.WriteLine("V"+ViewBag.DeleteFlag);

            ViewBag.Roles = Roles;

            return View();
        }



        [HttpPost]
        public IActionResult AddEvaluationDetails()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            Dblib dblib = new Dblib();

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Evaluation");

            int TenderID = int.Parse(Request.Form["TenderID"]);

            String[] CriteriaCategory = Request.Form["CriteriaCategory[]"];
            int cnt = CriteriaCategory.Length;

            String[] Other = Request.Form["Other[]"];

            String[] Criteria = Request.Form["Criteria[]"];

            String[] WeightageMarks = Request.Form["WeightageMarks[]"];
            String[] VCSScore = Request.Form["VCSScore[]"];

            String[] Satisfied = Request.Form["Satisfied[]"];

            String[] Remark = Request.Form["Remark[]"];


            Console.WriteLine(TenderID);
            for (int i = 0; i < cnt; i++)
            {
                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_Evaluation");
                    cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                    cmdB.Parameters.AddWithValue("@varCriteriaCategory", CriteriaCategory[i]);
                    cmdB.Parameters.AddWithValue("@varCriteria", Criteria[i]);
                    cmdB.Parameters.AddWithValue("@varWeightageMarks", WeightageMarks[i]);
                    cmdB.Parameters.AddWithValue("@varOther", Other[i]);

                    cmdB.Parameters.AddWithValue("@varVCSScore", VCSScore[i]);
                    cmdB.Parameters.AddWithValue("@varIsSatisfied", Satisfied[i]);
                    cmdB.Parameters.AddWithValue("@varRemark", Remark[i]);


                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();



                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);

                }
                finally
                {
                    cmdB.Dispose();

                    conn.Close();

                }
            }
            return RedirectToAction("TenderDetails", new { ID = TenderID });
        }


        [HttpPost]
        public IActionResult AddBECDetails()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            Dblib dblib = new Dblib();


            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "BEC");

            int TenderID = int.Parse(Request.Form["TenderID"]);

            String[] CriteriaCategory = Request.Form["CriteriaCategory[]"];
            int cnt = CriteriaCategory.Length;


            String[] Criteria = Request.Form["Criteria[]"];

            String[] Other = Request.Form["Other[]"];

            String[] Satisfied = Request.Form["Satisfied[]"];

            String[] Remark = Request.Form["Remark[]"];


            Console.WriteLine(TenderID);
            for (int i = 0; i < cnt; i++)
            {
                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_BEC");
                    cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                    cmdB.Parameters.AddWithValue("@varCriteriaCategory", CriteriaCategory[i]);
                    cmdB.Parameters.AddWithValue("@varCriteria", Criteria[i]);
                    cmdB.Parameters.AddWithValue("@varOther", Other[i]);

                    cmdB.Parameters.AddWithValue("@varIsSatisfied", Satisfied[i]);
                    cmdB.Parameters.AddWithValue("@varRemark", Remark[i]);


                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();



                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);

                }
                finally
                {
                    cmdB.Dispose();

                    conn.Close();

                }
            }
            return RedirectToAction("TenderDetails", new { ID = TenderID });
        }

        [HttpPost]
        public IActionResult AddSWOT()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            Dblib dblib = new Dblib();

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "SWOT");

            int TenderID = int.Parse(Request.Form["TenderID"]);

            String Strength = Request.Form["Strength"];


            String Weakness = Request.Form["Weakness"];

            String Threat = Request.Form["Threat"];

            String Oppurtunity = Request.Form["Oppurtunity"];



        
                try
                {
                    conn.Open();
                    cmdB = new SqlCommand();

                    cmdB.CommandType = CommandType.StoredProcedure;
                    cmdB.CommandText = "PROC_PMS_Add_Data";
                    cmdB.Parameters.AddWithValue("@varStepName", "Add_SWOT");
                    cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                    cmdB.Parameters.AddWithValue("@varStrengh", Strength);
                    cmdB.Parameters.AddWithValue("@varWeakness", Weakness);
                    cmdB.Parameters.AddWithValue("@varOppurtunities", Oppurtunity);

                    cmdB.Parameters.AddWithValue("@varThreats", Threat);
                    cmdB.Parameters.AddWithValue("@varCreatedBy", userId);


                    cmdB.Connection = conn;
                    cmdB.ExecuteNonQuery();



                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);

                }
                finally
                {
                    cmdB.Dispose();

                    conn.Close();

                }
            
            return RedirectToAction("TenderDetails", new { ID = TenderID });
        }


        [HttpPost]
        public IActionResult UpdateSWOT()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            Dblib dblib = new Dblib();
            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "SWOT");


            int TenderID = int.Parse(Request.Form["TenderID"]);
            int ID = int.Parse(Request.Form["SID"]);
            Console.WriteLine("SI"+ID);
            String Strength = Request.Form["Strength"];


            String Weakness = Request.Form["Weakness"];

            String Threat = Request.Form["Threat"];

            String Oppurtunity = Request.Form["Oppurtunity"];




            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Update_SWOT");
                cmdB.Parameters.AddWithValue("@varID", ID);
                cmdB.Parameters.AddWithValue("@varStrengh", Strength);
                cmdB.Parameters.AddWithValue("@varWeakness", Weakness);
                cmdB.Parameters.AddWithValue("@varOppurtunities", Oppurtunity);

                cmdB.Parameters.AddWithValue("@varThreats", Threat);


                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();



            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }

            return RedirectToAction("TenderDetails", new { ID = TenderID });
        }


        [HttpPost]
        public IActionResult UpdateTenderDetails(Tender tender)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            Dblib dblib = new Dblib();


            String FinalTenderNFDate = "";
            String FinalBidSDateOn = "";
            String FinalBidSDateOff = "";
            String FinalBidOpeningDate = "";
            String FinalPreBidDate = "";

            int TenderID = int.Parse(Request.Form["TenderID"]);


            String ClientName = Request.Form["ClientName"];

            String TenderNo = Request.Form["TenderNo"];
            String TenderName = Request.Form["TenderName"];
            String ScopeOfWork = Request.Form["ScopeOfWork"];
            int Category = int.Parse(Request.Form["TenderCategory"]);

            String Region = Request.Form["Region"];
            String Source = Request.Form["Source"];
            String SourceDetails = Request.Form["SourceDetails"];

            String TenderNFDate = Request.Form["TenderNFDate"];
            String BidSDateOn = Request.Form["BidSDateOn"];
            String BidSDateOff = Request.Form["BidSDateOff"];

            String BidSTimeOn = Request.Form["BidSTimeOn"];
            String BidSTimeOff = Request.Form["BidSTimeOff"];

            String PreBidDate = Request.Form["PreBidDate"];
            String PreBidTime = Request.Form["PreBidTime"];
            String ContactDetails = Request.Form["ContactDetails"];
            String Remark = Request.Form["Remark"];
            String Designation = Request.Form["Designation"];
            String Email = Request.Form["Email"];

            String PhoneNumber = Request.Form["PhoneNumber"];

            String BidOpeningDate = Request.Form["BidOpeningDate"];

            String BidOpeningTime = Request.Form["BidOpeningTime"];

            String EMD = Request.Form["EMD"];

            String EMDE = Request.Form["EMDE"];

            String TenderFeeDetails = Request.Form["TenderFeeDetails"];
            String BidValidity = Request.Form["BidValidity"];


            String TEx = Request.Form["TEx"];

            String ContractValue = Request.Form["ContractValue"];

            String ContractPeriod = Request.Form["ContractPeriod"];

            String ExContractPeriod = Request.Form["ExContractPeriod"];

            int Priority = int.Parse(Request.Form["Priority"]);

            int Industry = int.Parse(Request.Form["Industry"]);

            int TenderCategory = int.Parse(Request.Form["TenderCategory"]);

            int TenderType = int.Parse(Request.Form["TenderType"]);

            int BiddingType = int.Parse(Request.Form["BiddingType"]);

            String Mode = Request.Form["Mode"];


            String JV = Request.Form["JV"];

            String BidAward = Request.Form["BidAward"];


            Double TenderFee = Double.Parse(Request.Form["TenderFee"]);


            if (PreBidDate != null)
            {

                FinalPreBidDate = dblib.getDate(PreBidDate);
            }
            if (TenderNFDate != "")
            {
                FinalTenderNFDate = dblib.getDate(TenderNFDate);
            }


            if (BidOpeningDate != "")
            {
                FinalBidOpeningDate = dblib.getDate(BidOpeningDate);
            }


            if (BidSDateOn != "")
            {
                FinalBidSDateOn = dblib.getDate(BidSDateOn);
            }


            if (BidSDateOff != "")
            {
                FinalBidSDateOff = dblib.getDate(BidSDateOff);
            }



            if (TenderNFDate != "")
            {
                FinalTenderNFDate = dblib.getDate(TenderNFDate);
            }


            if (BidSDateOn != "")
            {
                FinalBidSDateOn = dblib.getDate(BidSDateOn);
            }


            if (BidSDateOff != "")
            {
                FinalBidSDateOff = dblib.getDate(BidSDateOff);
            }


            try
            {
                conn.Open();
                cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Add_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Update_Tender");
                cmdB.Parameters.AddWithValue("@varTenderID", TenderID);
                cmdB.Parameters.AddWithValue("@varTenderNo", TenderNo);
                cmdB.Parameters.AddWithValue("@varClientName", ClientName);
                cmdB.Parameters.AddWithValue("@varTenderName", TenderName);

                cmdB.Parameters.AddWithValue("@varScope", ScopeOfWork);

                cmdB.Parameters.AddWithValue("@varCategory", Category);
                cmdB.Parameters.AddWithValue("@varRegion", Region);
                cmdB.Parameters.AddWithValue("@varSource", Source);
                cmdB.Parameters.AddWithValue("@varTenderFee", TenderFee);

                cmdB.Parameters.AddWithValue("@varSourceDetails", SourceDetails);
                cmdB.Parameters.AddWithValue("@varTenderNFDate", FinalTenderNFDate);
                cmdB.Parameters.AddWithValue("@varBillSubDateOnline", FinalBidSDateOn);
                cmdB.Parameters.AddWithValue("@varBillSubDateOffline", FinalBidSDateOff);
                cmdB.Parameters.AddWithValue("@varStatus", "Prospective");
                cmdB.Parameters.AddWithValue("@varContactDetails", ContactDetails);
                cmdB.Parameters.AddWithValue("@varRemark", Remark);
                cmdB.Parameters.AddWithValue("@varTenderType", TenderType);
                cmdB.Parameters.AddWithValue("@varPreBidDate", FinalPreBidDate);
                cmdB.Parameters.AddWithValue("@varPreBidTime", PreBidTime);
                cmdB.Parameters.AddWithValue("@varBidTimeOl", BidSTimeOn);
                cmdB.Parameters.AddWithValue("@varBidTimeOf", BidSTimeOff);
                cmdB.Parameters.AddWithValue("@varBidValidity", BidValidity);

                cmdB.Parameters.AddWithValue("@varBidOpeningTime", BidOpeningTime);
                cmdB.Parameters.AddWithValue("@varBidOpeningDate", FinalBidOpeningDate);
                cmdB.Parameters.AddWithValue("@varDesignation", Designation);
                cmdB.Parameters.AddWithValue("@varEmail", Email);
                cmdB.Parameters.AddWithValue("@varPhoneNumber", PhoneNumber);
                cmdB.Parameters.AddWithValue("@varEMD", EMD);
                cmdB.Parameters.AddWithValue("@varEMDFlag", EMDE);
                cmdB.Parameters.AddWithValue("@varTenderFeeDetails", TenderFeeDetails);
                cmdB.Parameters.AddWithValue("@varTenderFeeExempted", TEx);
                cmdB.Parameters.AddWithValue("@varContractValue", ContractValue);
                cmdB.Parameters.AddWithValue("@varPriority", Priority);
                cmdB.Parameters.AddWithValue("@varContractPeriod", ContractPeriod);
                cmdB.Parameters.AddWithValue("@varExContractPeriod", ExContractPeriod);
                cmdB.Parameters.AddWithValue("@varIndustry", Industry);
                cmdB.Parameters.AddWithValue("@varTenderCategory", TenderCategory);
                cmdB.Parameters.AddWithValue("@varBiddingType", BiddingType);
                cmdB.Parameters.AddWithValue("@varMode", Mode);
                cmdB.Parameters.AddWithValue("@varJV", JV);
                cmdB.Parameters.AddWithValue("@varBidAward", BidAward);


                cmdB.Parameters.AddWithValue("@varModifiedBy", userId);

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();



            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }

            HttpContext.Session.Remove("N");

            HttpContext.Session.SetString("N", "Basic Info");
            return RedirectToAction("TenderDetails", new { ID = TenderID });
            //return RedirectToAction("Tenders");
        }

        public IActionResult Locations()
        {
            List<Locations> Locations = new List<Locations>();

            //SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            //conn.Open();
            //SqlCommand cmdB = new SqlCommand();
            //cmdB.CommandType = CommandType.StoredProcedure;
            //cmdB.CommandText = "PROC_TH_SELECT_Master_Data";
            //cmdB.Parameters.AddWithValue("@varStepName", "Select_Location");
            //cmdB.Connection = conn;

            //SqlDataReader drB = cmdB.ExecuteReader();

            //while (drB.Read())
            //{
            //    Locations.Add(new Locations
            //    {
            //        ID = int.Parse(drB["ID"].ToString()),
            //        LocID = int.Parse(drB["LocID"].ToString()),
            //        Type = drB["LocType"].ToString(),
            //        Name = drB["Name"].ToString(),
            //        SectorName = drB["SectorName"].ToString(),
            //        CountryName = drB["CountryName"].ToString(),
            //        StateName = drB["StateName"].ToString(),
            //        CategoryName = drB["CategoryName"].ToString(),
            //        MinStay = Double.Parse(drB["MinStay"].ToString()),
            //        TourType = drB["TourType"].ToString(),
            //        LocationInfo = drB["LocInfo"].ToString(),

            //    });
            //}

            ViewBag.Locations = Locations;
            //drB.Dispose();
            //cmdB.Dispose();
            //conn.Close();
            return View();

        }

        public IActionResult Hotels()
        {
            List<Hotels> Hotels= new List<Hotels>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Hotel_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Hotels");
            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Hotels.Add(new Hotels
                {
                    ID = int.Parse(drB["ID"].ToString()),
                    HID = int.Parse(drB["HID"].ToString()),

                    GroupOf = drB["GroupOf"].ToString(),
                    Name = drB["Name"].ToString(),
                    SectorName = drB["Sector"].ToString(),
                    Address1 = drB["Address1"].ToString(),
                    Address1T = drB["Address1Town"].ToString(),
                    Address1C = drB["Address1Country"].ToString(),
                    Address2T = drB["Address2Town"].ToString(),
                    Address2C = drB["Address2Country"].ToString(),
                    Address2 = drB["Address2"].ToString(),
                    HotelCat = drB["HotelCategory"].ToString(),
                    ExtraAdultCharges = Double.Parse(drB["ExtraAdultCharges"].ToString()),
                    ExtraChildCharges = Double.Parse(drB["ExtraChildCharges"].ToString()),
                    ExtraMealCharges = Double.Parse(drB["ExtraMealCharges"].ToString()),

                    Star = drB["StarCategory"].ToString(),
                    ContactDesignation = drB["ContactDesignation"].ToString(),
                    ContactMail = drB["ContactMailID"].ToString(),
                    ContactMobile = drB["ContactMobile"].ToString(),
                    ContactLandLine = drB["ContactLandLine"].ToString(),
                    ContactPerMob = drB["ContactPerMob"].ToString(),
                    ContactWorkProfile = drB["ContactWorProfile"].ToString(),
                    Meal = drB["MealPlan"].ToString(),
                    IsFour = drB["IsFourPerson"].ToString(),
                    CompanyFrom = int.Parse(drB["CompanyFrom"].ToString()),
                    CategoryName = drB["Category"].ToString(),
                    ReferenceName = drB["Reference"].ToString(),
                    AccrediationName = drB["Accrediation"].ToString(),
                    ReservationContact = drB["Reservation Contact"].ToString(),
                    OwnVisit = drB["OwnVisit"].ToString(),
                    RoomTypes = drB["RoomType"].ToString(),
                    Grade = drB["Grade"].ToString(),
                    Tarrif = drB["TarrifPDF"].ToString(),


                });
            }

            ViewBag.Hotels = Hotels;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();
            return View();

        }


        public IActionResult DMC()
        {
            List<DMC> DMC = new List<DMC>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Select_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_DMC");
            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                DMC.Add(new DMC
                {
                    DID = int.Parse(drB["DID"].ToString()),

                    GroupOf = drB["GroupOf"].ToString(),
                    CompanyName = drB["Name"].ToString(),
                    SectorName = drB["SecWork"].ToString(),
                    SpecificSp = drB["SpecificSP"].ToString(),
                    ServiceIn = drB["ServiceIn"].ToString(),
                    Address1 = drB["Address1"].ToString(),
                    Address2 = drB["Address2"].ToString(),

                    OwnFleets = drB["OwnFleets"].ToString(),

                    CompanyFrom = int.Parse(drB["EstFrom"].ToString()),
          
                    ReferenceName = drB["Reference"].ToString(),
                    AccrediationName = drB["Accrediation"].ToString(),
                    ReservationContact = drB["EmerContact"].ToString(),
               
                    Grade = drB["Grade"].ToString(),
        

                });
            }

            ViewBag.DMC = DMC;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            return View();

        }


        public IActionResult ViewDMC(int? ID)
        {
            List<DMC> DMC = new List<DMC>();
            List<DMC> Contact = new List<DMC>();
            List<DMC> Bank = new List<DMC>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Select_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_DMC_By_ID");
            cmdB.Parameters.AddWithValue("@varDID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                DMC.Add(new DMC
                {
                    DID = int.Parse(drB["DID"].ToString()),

                    GroupOf = drB["GroupOf"].ToString(),
                    CompanyName = drB["Name"].ToString(),
                    SectorName = drB["SecWork"].ToString(),
                    SpecificSp = drB["SpecificSP"].ToString(),
                    ServiceIn = drB["ServiceIn"].ToString(),
                    Address1 = drB["Address1"].ToString(),
                    Address2 = drB["Address2"].ToString(),

                    OwnFleets = drB["OwnFleets"].ToString(),

                    CompanyFrom = int.Parse(drB["EstFrom"].ToString()),

                    ReferenceName = drB["Reference"].ToString(),
                    AccrediationName = drB["Accrediation"].ToString(),
                    ReservationContact = drB["EmerContact"].ToString(),

                    Grade = drB["Grade"].ToString(),


                });
            }


            ViewBag.DMC = DMC;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Select_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_DMCBank_By_ID");
            cmdB.Parameters.AddWithValue("@varDID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Bank.Add(new DMC
                {
                    DID = int.Parse(drB["DID"].ToString()),

                    BankName = drB["BankName"].ToString(),
                    AccountNo = drB["AccNo"].ToString(),
                    Branch = drB["Branch"].ToString(),
                    IFSC = drB["IFSC"].ToString(),


                });
            }

            ViewBag.Bank = Bank;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();






            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Select_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_DMCContacts_By_ID");
            cmdB.Parameters.AddWithValue("@varDID", ID);

            cmdB.Connection = conn;

            drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Contact.Add(new DMC
                {
                    DID = int.Parse(drB["DID"].ToString()),
                    ContactName = drB["Name"].ToString(),
                    ContactType = drB["Type"].ToString(),

                    ContactDesignation = drB["Designation"].ToString(),
                    ContactMail = drB["MailID"].ToString(),
                    ContactMobile = drB["Mobile"].ToString(),
                    ContactLandLine = drB["OfficeLandLine"].ToString(),
                    ContactPerMob = drB["PersonalMob"].ToString(),
                    ContactWorkProfile = drB["WorkProfile"].ToString(),


                });
            }

            ViewBag.Contacts = Contact;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();




            return View();
        }


        public IActionResult AddLocation()
        {

            return View();
        }

        public IActionResult AddHotel()
        {

            return View();
        }

        public IActionResult AddDMC()
        {

            return View();
        }

        public IActionResult AddVisa()
        {

            return View();
        }

        public IActionResult VisaAssistance()
        {
            List<VisaAssistance> Visa = new List<VisaAssistance>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_VisaAssistance";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_VisaAssistance");

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Visa.Add(new VisaAssistance
                {
                    VAID = int.Parse(drB["VAID"].ToString()),

                    CountryName = drB["CountryName"].ToString(),
                    Continent = drB["Continent"].ToString(),
                    GuestProfession = drB["GuestProfession"].ToString(),
                    VisaType = drB["VisaType"].ToString(),
                    Fees = drB["Fees"].ToString(),
                    SOP = drB["SOP"].ToString(),
                    ImportantNote = drB["ImpNote"].ToString(),

                    Duration = drB["Duration"].ToString(),



                });
            }


            ViewBag.Visa = Visa;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            return View();
        }


        public IActionResult ViewVisa(int ID)
        {
            List<VisaAssistance> Visa = new List<VisaAssistance>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_VisaAssistance";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_VisaAssistance_ByID");
            cmdB.Parameters.AddWithValue("@varVAID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Visa.Add(new VisaAssistance
                {
                    VAID = int.Parse(drB["VAID"].ToString()),

                    CountryName = drB["CountryName"].ToString(),
                    Continent = drB["Continent"].ToString(),
                    GuestProfession = drB["GuestProfession"].ToString(),
                    VisaType = drB["VisaType"].ToString(),
                    Fees = drB["Fees"].ToString(),
                    SOP = drB["SOP"].ToString(),
                    ImportantNote = drB["ImpNote"].ToString(),

                    Duration = drB["Duration"].ToString(),



                });
            }


            ViewBag.Visa = Visa;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();

            return View();
        }

      


        [HttpPost]
        public IActionResult AddLocationDetails(Locations model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();


            String Type = Request.Form["Type"];
            String Country = Request.Form["Country"];
            String State = Request.Form["StateName"];
            String Sector = Request.Form["SectorName"];
            
            String LocationInfo = Request.Form["LocationInfo"];

            String Location = Request.Form["LocationName"];
            String Category = Request.Form["Category"];
            String TourType = Request.Form["TourType"];
            Double MinStay = Double.Parse(Request.Form["MinStay"]);

            int LocID = 0;
            try
            {
                conn.Open();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TH_Add_Master_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Location");
                cmdB.Parameters.AddWithValue("@varName", Location);
                cmdB.Parameters.AddWithValue("@varSectorName", Sector);
                cmdB.Parameters.AddWithValue("@varCountryName", Country);
                cmdB.Parameters.AddWithValue("@varStateName", State);

                cmdB.Parameters.AddWithValue("@varCategory", Category);
                cmdB.Parameters.AddWithValue("@varMinStay", MinStay);
                cmdB.Parameters.AddWithValue("@varTourType", TourType);
                cmdB.Parameters.AddWithValue("@varType", Type);
                cmdB.Parameters.AddWithValue("@varLocInfo", LocationInfo);

                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                cmdB.Connection = conn;
                SqlDataReader drB = cmdB.ExecuteReader();
                while (drB.Read())
                {
                    LocID = int.Parse(drB["LocID"].ToString());

                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }


            String[] DestinationName = Request.Form["DestinationName[]"];
            int cnt = DestinationName.Length;


            String[] DestCat = Request.Form["DestCat[]"];
            int cnt2 = DestCat.Length;



            String[] ORoadKM = Request.Form["ORoadKM[]"];
            int cnt3 = ORoadKM.Length;


            String[] OJourneyHR = Request.Form["OJourneyHR[]"];
            int cnt4 = OJourneyHR.Length;



            String[] MDestinationName = Request.Form["MDestinationName[]"];
            int cnt5 = MDestinationName.Length;


            String[] MInformation = Request.Form["MInformation[]"];
            int cnt6 = MInformation.Length;



            String[] MRoadKM = Request.Form["MRoadKM[]"];
            int cnt7 = MRoadKM.Length;


            String[] MJourneyHR = Request.Form["MJourneyHR[]"];
            int cnt8 = MJourneyHR.Length;




            String[] OpDestinationName = Request.Form["OpDestinationName[]"];
            int cnt9 = OpDestinationName.Length;


            String[] OpInformation = Request.Form["OpInformation[]"];
            int cnt10 = OpInformation.Length;



            String[] OpRoadKM = Request.Form["OpRoadKM[]"];
            int cnt11 = OpRoadKM.Length;


            String[] OpJourneyHR = Request.Form["OpJourneyHR[]"];
            int cnt12 = OpJourneyHR.Length;



            String[] EDestinationName = Request.Form["EDestinationName[]"];
            int cnt13 = EDestinationName.Length;


            String[] EInformation = Request.Form["EInformation[]"];
            int cnt14 = EInformation.Length;



            String[] ERoadKM = Request.Form["ERoadKM[]"];
            int cnt15 = ERoadKM.Length;


            String[] EJourneyHR = Request.Form["EJourneyHR[]"];
            int cnt16 = EJourneyHR.Length;

            if (LocID != 0)
            {
                for (int i = 0; i < cnt; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_ConnectedDestination");
                        cmdB.Parameters.AddWithValue("@varLocID", LocID);
                        cmdB.Parameters.AddWithValue("@varPlace", DestinationName[i]);
                        cmdB.Parameters.AddWithValue("@varCategory", DestCat[i]);
                        cmdB.Parameters.AddWithValue("@varRoadD", Double.Parse(ORoadKM[i]));
                        cmdB.Parameters.AddWithValue("@varJourneyH", Double.Parse(OJourneyHR[i]));
                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e) {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }



                for (int i = 0; i < cnt5; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_MustSeenSight");
                        cmdB.Parameters.AddWithValue("@varLocID", LocID);
                        cmdB.Parameters.AddWithValue("@varName", MDestinationName[i]);
                        cmdB.Parameters.AddWithValue("@varInformation", MInformation[i]);
                        cmdB.Parameters.AddWithValue("@varRoadD", Double.Parse(ORoadKM[i]));
                        cmdB.Parameters.AddWithValue("@varJourneyH", Double.Parse(OJourneyHR[i]));
                        cmdB.Parameters.AddWithValue("@varPhoto", null);
                        cmdB.Parameters.AddWithValue("@varVideo", null);

                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }



                for (int i = 0; i < cnt9; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_OptionalSeenSight");
                        cmdB.Parameters.AddWithValue("@varLocID", LocID);
                        cmdB.Parameters.AddWithValue("@varName", OpDestinationName[i]);
                        cmdB.Parameters.AddWithValue("@varInformation", OpInformation[i]);
                        cmdB.Parameters.AddWithValue("@varRoadD", Double.Parse(OpRoadKM[i]));
                        cmdB.Parameters.AddWithValue("@varJourneyH", Double.Parse(OpJourneyHR[i]));
                        cmdB.Parameters.AddWithValue("@varPhoto", null);
                        cmdB.Parameters.AddWithValue("@varVideo", null);

                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }



                for (int i = 0; i < cnt13; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_ExcusrionSeenSight");
                        cmdB.Parameters.AddWithValue("@varLocID", LocID);
                        cmdB.Parameters.AddWithValue("@varName", EDestinationName[i]);
                        cmdB.Parameters.AddWithValue("@varInformation", EInformation[i]);
                        cmdB.Parameters.AddWithValue("@varRoadD", Double.Parse(ERoadKM[i]));
                        cmdB.Parameters.AddWithValue("@varJourneyH", Double.Parse(EJourneyHR[i]));
                        cmdB.Parameters.AddWithValue("@varPhoto", null);
                        cmdB.Parameters.AddWithValue("@varVideo", null);

                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }
            }


            if (LocID == 0)
            {
                Message = "Failed";
            }
            else
            {
                Message = "Success";

            }
            return RedirectToAction("Locations");
        }


        [HttpPost]
        public IActionResult AddVisa(VisaAssistance model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();

            Console.WriteLine("hi");
            
            String Continent = Request.Form["Continent"];
            String CountryName = Request.Form["CountryName"];
            String GuestProfession = Request.Form["GuestProfession"];
            String Fees = Request.Form["Fees"];
            String VisaType = Request.Form["VisaType"];

            String Duration = Request.Form["Duration"];

            String SOP = Request.Form["SOP"];
            String ImportantNote = Request.Form["ImportantNote"];

            try
            {
                conn.Open();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TH_VisaAssistance";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_VisaAssistance");
                cmdB.Parameters.AddWithValue("@varContinent", Continent);
                cmdB.Parameters.AddWithValue("@varCountryName", CountryName);
                cmdB.Parameters.AddWithValue("@varGuestProfession", GuestProfession);
                cmdB.Parameters.AddWithValue("@varVisaType", VisaType);

                cmdB.Parameters.AddWithValue("@varFees", int.Parse(Fees));
                cmdB.Parameters.AddWithValue("@varDuration", Duration);
                cmdB.Parameters.AddWithValue("@varSOP", SOP);
                cmdB.Parameters.AddWithValue("@varImpNote", ImportantNote);

                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                cmdB.Connection = conn;
                cmdB.ExecuteNonQuery();


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }


            
            return RedirectToAction("VisaAssistance");
        }


        [HttpPost]
        public IActionResult AddHotelDetails(Locations model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();


            String Name = Request.Form["Name"];
            String GroupOf = Request.Form["GroupOf"];
            String SectorName = Request.Form["SectorName"];

            String Address1 = Request.Form["Address1"];
            String Address2 = Request.Form["Address2"];
            String Address1T = Request.Form["Address1T"];
            String Address1C = Request.Form["Address1C"];
            String Address2C = Request.Form["Address2C"];
            String Address2T = Request.Form["Address2T"];


            String Star = Request.Form["Star"];
            String HotelCat = Request.Form["HotelCat"];
            String ContactName = Request.Form["ContactName"];
            String ContactMobile = Request.Form["ContactMobile"];
            String ContactDesignation = Request.Form["ContactDesignation"];
            String ContactLandLine = Request.Form["ContactLandLine"];
            String ContactMail = Request.Form["ContactMail"];
            String ContactPerMob = Request.Form["ContactPerMob"];
            String ContactWorkP = Request.Form["ContactWorkProfile"];

            String ReservationContact = Request.Form["ReservationContact"];
            String IsFour = Request.Form["IsFour"];
            String CompanyFrom = Request.Form["CompanyFrom"];
            String Meal = Request.Form["Meal"];
            String Category = Request.Form["Category"];
            String ReferenceName = Request.Form["ReferenceName"];
            String AccrediationName = Request.Form["AccrediationName"];


            String OwnVisit = Request.Form["OwnVisit"];
            String RoomTypes = Request.Form["RoomTypes"];
            String Grade = Request.Form["Grade"];
            Double ExtraAdultCharges = Double.Parse(Request.Form["ExtraAdultCharges"]);
            Double ExtraChildCharges = Double.Parse(Request.Form["ExtraChildCharges"]);
            Double ExtraMealCharges = Double.Parse(Request.Form["ExtraMealCharges"]);



            int HID = 0;
            try
            {
                conn.Open();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TH_Hotel_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_Hotels");
                cmdB.Parameters.AddWithValue("@varName", Name);
                cmdB.Parameters.AddWithValue("@varGroupOf", GroupOf);
                cmdB.Parameters.AddWithValue("@varSector", SectorName);
                cmdB.Parameters.AddWithValue("@varAddress1", Address1);

                cmdB.Parameters.AddWithValue("@varAddress1Country", Address1C);
                cmdB.Parameters.AddWithValue("@varAddress1Town", Address1T);
                cmdB.Parameters.AddWithValue("@varAddress2", Address2);
                cmdB.Parameters.AddWithValue("@varAddress2Country", Address2C);
                cmdB.Parameters.AddWithValue("@varAddress2Town", Address2T);
                cmdB.Parameters.AddWithValue("@varStarCategory", Star);
                cmdB.Parameters.AddWithValue("@varHotelCategory", HotelCat);
                cmdB.Parameters.AddWithValue("@varContactName", ContactName);
                cmdB.Parameters.AddWithValue("@varContactDesignation", ContactDesignation);
                cmdB.Parameters.AddWithValue("@varContactMobile", ContactMobile);
                cmdB.Parameters.AddWithValue("@varContactLandLine", ContactLandLine);
                cmdB.Parameters.AddWithValue("@varContactMailID", ContactMail);
                cmdB.Parameters.AddWithValue("@varContactPerMob", ContactPerMob);
                cmdB.Parameters.AddWithValue("@varContactWorProfile", ContactWorkP);

                cmdB.Parameters.AddWithValue("@varReservationContact", ReservationContact);

                cmdB.Parameters.AddWithValue("@varMealPlan", Meal);
                cmdB.Parameters.AddWithValue("@varIsFourPerson", IsFour);
                cmdB.Parameters.AddWithValue("@varCompanyFrom", CompanyFrom);
                cmdB.Parameters.AddWithValue("@varCategory", Category);

                cmdB.Parameters.AddWithValue("@varReference", ReferenceName);
                cmdB.Parameters.AddWithValue("@varAccrediation", AccrediationName);
                cmdB.Parameters.AddWithValue("@varOwnVisit", OwnVisit);
                cmdB.Parameters.AddWithValue("@varRoomType", RoomTypes);
                cmdB.Parameters.AddWithValue("@varGrade", Grade);
                cmdB.Parameters.AddWithValue("@varTarrifPDF", null);
                cmdB.Parameters.AddWithValue("@varExtraAdultCharges", ExtraAdultCharges);
                cmdB.Parameters.AddWithValue("@varExtraChildCharges", ExtraChildCharges);
                cmdB.Parameters.AddWithValue("@varExtraMealCharges", ExtraMealCharges);


                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                cmdB.Connection = conn;
                SqlDataReader drB = cmdB.ExecuteReader();
                while (drB.Read())
                {
                    HID = int.Parse(drB["HID"].ToString());

                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }


            String[] BankName = Request.Form["BankName[]"];
            int cnt = BankName.Length;


            String[] AccountNo = Request.Form["AccountNo[]"];
            int cnt2 = AccountNo.Length;



            String[] IFSC = Request.Form["IFSC[]"];
            int cnt3 = IFSC.Length;


            String[] Branch = Request.Form["Branch[]"];
            int cnt4 = Branch.Length;



            if (HID != 0)
            {
                for (int i = 0; i < cnt; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Hotel_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_HotelBank");
                        cmdB.Parameters.AddWithValue("@varHID", HID);
                        cmdB.Parameters.AddWithValue("@varName", BankName[i]);
                        cmdB.Parameters.AddWithValue("@varAccNo", AccountNo[i]);
                        cmdB.Parameters.AddWithValue("@varIFSC", IFSC[i]);
                        cmdB.Parameters.AddWithValue("@varBranch", Branch[i]);
                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }



            }


            if (HID == 0)
            {
                Message = "Failed";
            }
            else
            {
                Message = "Success";

            }
            return RedirectToAction("Hotels");
        }


        [HttpPost]
        public IActionResult AddDMCDetails(DMC model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId
            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            SqlCommand cmdB = new SqlCommand();


            String CompanyName = Request.Form["CompanyName"];
            String GroupOf = Request.Form["GroupOf"];
            String SectorName = Request.Form["SectorName"];

            String Address1 = Request.Form["Address1"];
            String Address2 = Request.Form["Address2"];
            String SpecificSP = Request.Form["SpecificSp"];

            String ServiceIn = Request.Form["ServiceIn"];
         


            String EmergencyContact = Request.Form["EmergencyContact"];


            String CompanyFrom = Request.Form["CompanyFrom"];

            String ReferenceName = Request.Form["ReferenceName"];
            String AccrediationName = Request.Form["AccrediationName"];


            String OwnFleets = Request.Form["OwnFleets"];
            String Grade = Request.Form["Grade"];


            int DID = 0;
            try
            {
                conn.Open();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_TH_Add_Master_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Add_DMC");
                cmdB.Parameters.AddWithValue("@varName", CompanyName);
                cmdB.Parameters.AddWithValue("@varGroupOf", GroupOf);
                cmdB.Parameters.AddWithValue("@varSecWork", SectorName);
                cmdB.Parameters.AddWithValue("@varAddress1", Address1);

                cmdB.Parameters.AddWithValue("@varAddress2", Address2);

                cmdB.Parameters.AddWithValue("@varSpecificSP", SpecificSP);
                cmdB.Parameters.AddWithValue("@varServiceIn", ServiceIn);
                cmdB.Parameters.AddWithValue("@varEstFrom", CompanyFrom);
                cmdB.Parameters.AddWithValue("@varOwnFleets", OwnFleets);

                cmdB.Parameters.AddWithValue("@varReference", ReferenceName);
                cmdB.Parameters.AddWithValue("@varAccrediation", AccrediationName);



                cmdB.Parameters.AddWithValue("@varEmerContact", EmergencyContact);



                cmdB.Parameters.AddWithValue("@varGrade", Grade);


                cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                cmdB.Connection = conn;
                SqlDataReader drB = cmdB.ExecuteReader();
                while (drB.Read())
                {
                    DID = int.Parse(drB["DID"].ToString());

                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {
                cmdB.Dispose();

                conn.Close();

            }


            String[] BankName = Request.Form["BankName[]"];
            int cnt = BankName.Length;


            String[] AccountNo = Request.Form["AccountNo[]"];
            int cnt2 = AccountNo.Length;



            String[] IFSC = Request.Form["IFSC[]"];
            int cnt3 = IFSC.Length;


            String[] Branch = Request.Form["Branch[]"];
            int cnt4 = Branch.Length;

            String[] ContactType = Request.Form["ContactType[]"];

            String[] ContactName = Request.Form["ContactName[]"];
            int cnt5 = ContactName.Length;

            String[] ContactMobile = Request.Form["ContactMobile[]"];
            String[] ContactDesignation = Request.Form["ContactDesignation[]"];
            String[] ContactLandLine = Request.Form["ContactLandLine[]"];
            String[] ContactMail = Request.Form["ContactMail[]"];
            String[] ContactPerMob = Request.Form["ContactPerMob[]"];
            String[] ContactWorkP = Request.Form["ContactWorkProfile[]"];

            if (DID != 0)
            {
                for (int i = 0; i < cnt; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_DMCBankDetails");
                        cmdB.Parameters.AddWithValue("@varDID", DID);
                        cmdB.Parameters.AddWithValue("@varName", BankName[i]);
                        cmdB.Parameters.AddWithValue("@varAccNo", AccountNo[i]);
                        cmdB.Parameters.AddWithValue("@varIFSC", IFSC[i]);
                        cmdB.Parameters.AddWithValue("@varBranch", Branch[i]);
                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);
                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }


                for (int i = 0; i < cnt5; i++)
                {
                    try
                    {
                        conn.Open();
                        cmdB = new SqlCommand();
                        cmdB.CommandType = CommandType.StoredProcedure;
                        cmdB.CommandText = "PROC_TH_Add_Master_Data";
                        cmdB.Parameters.AddWithValue("@varStepName", "Add_DMCContacts");
                        cmdB.Parameters.AddWithValue("@varDID", DID);
                        cmdB.Parameters.AddWithValue("@varDesignation", ContactDesignation[i]);
                        cmdB.Parameters.AddWithValue("@varType", ContactType[i]);

                        cmdB.Parameters.AddWithValue("@varName", ContactName[i]);
                        cmdB.Parameters.AddWithValue("@varMobile", ContactMobile[i]);
                        cmdB.Parameters.AddWithValue("@varOfficeLandLine", ContactLandLine[i]);
                        cmdB.Parameters.AddWithValue("@varMailID", ContactMail[i]);
                        cmdB.Parameters.AddWithValue("@varPersonalMob", ContactPerMob[i]) ;
                        cmdB.Parameters.AddWithValue("@varWorkProfile", ContactWorkP[i]);
                        cmdB.Parameters.AddWithValue("@varCreatedBy", userId);

                        cmdB.Connection = conn;
                        cmdB.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.ToString());
                    }
                    cmdB.Dispose();
                    conn.Close();

                }



            }


            if (DID == 0)
            {
                Message = "Failed";
            }
            else
            {
                Message = "Success";

            }
            return RedirectToAction("DMC");
        }


        public IActionResult ViewHotel(int? ID)
        {
            List<Hotels>  Hotels =  new List<Hotels>();
            List<Hotels> Bank = new List<Hotels>();

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Hotel_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_Hotels_By_ID");
            cmdB.Parameters.AddWithValue("@varHID", ID);

            cmdB.Connection = conn;

            SqlDataReader drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Hotels.Add(new Hotels
                {
                    ID = int.Parse(drB["ID"].ToString()),
                    HID = int.Parse(drB["HID"].ToString()),

                    GroupOf = drB["GroupOf"].ToString(),
                    Name = drB["Name"].ToString(),
                    SectorName = drB["Sector"].ToString(),
                    Address1 = drB["Address1"].ToString(),
                    Address1T = drB["Address1Town"].ToString(),
                    Address1C = drB["Address1Country"].ToString(),
                    Address2T = drB["Address2Town"].ToString(),
                    Address2C = drB["Address2Country"].ToString(),
                    Address2 = drB["Address2"].ToString(),
                    HotelCat = drB["HotelCategory"].ToString(),
                    ExtraAdultCharges = Double.Parse(drB["ExtraAdultCharges"].ToString()),
                    ExtraChildCharges = Double.Parse(drB["ExtraChildCharges"].ToString()),
                    ExtraMealCharges = Double.Parse(drB["ExtraMealCharges"].ToString()),

                    Star = drB["StarCategory"].ToString(),
                    ContactDesignation = drB["ContactDesignation"].ToString(),
                    ContactMail = drB["ContactMailID"].ToString(),
                    ContactMobile = drB["ContactMobile"].ToString(),
                    ContactLandLine = drB["ContactLandLine"].ToString(),
                    ContactPerMob = drB["ContactPerMob"].ToString(),
                    ContactWorkProfile = drB["ContactWorProfile"].ToString(),
                    Meal = drB["MealPlan"].ToString(),
                    IsFour = drB["IsFourPerson"].ToString(),
                    CompanyFrom = int.Parse(drB["CompanyFrom"].ToString()),
                    CategoryName = drB["Category"].ToString(),
                    ReferenceName = drB["Reference"].ToString(),
                    AccrediationName = drB["Accrediation"].ToString(),
                    ReservationContact = drB["Reservation Contact"].ToString(),
                    OwnVisit = drB["OwnVisit"].ToString(),
                    RoomTypes = drB["RoomType"].ToString(),
                    Grade = drB["Grade"].ToString(),


                });
            }

            ViewBag.Hotels = Hotels;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



            conn.Open();
            cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Hotel_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Select_HotelBankDetails_By_ID");
            cmdB.Parameters.AddWithValue("@varHID", ID);

            cmdB.Connection = conn;

           drB = cmdB.ExecuteReader();

            while (drB.Read())
            {
                Bank.Add(new Hotels
                {
                    HID = int.Parse(drB["HID"].ToString()),
                
                    BankName = drB["HotelBankName"].ToString(),
                    AccountNo = drB["AccNo"].ToString(),
                    Branch = drB["Branch"].ToString(),
                    IFSC = drB["IFSC"].ToString(),


                });
            }

            ViewBag.Bank = Bank;
            drB.Dispose();
            cmdB.Dispose();
            conn.Close();



  
            return View();
        }


        public IActionResult DeleteLocation(int? ID)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Delete_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_Location");
            cmdB.Parameters.AddWithValue("@varLocID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("Locations");
        }


        public IActionResult DeleteHotel(int? ID)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Hotel_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_Hotel");
            cmdB.Parameters.AddWithValue("@varHID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("Hotels");
        }

        public IActionResult DeleteDMC(int? ID)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_Delete_Master_Data";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_DMC");
            cmdB.Parameters.AddWithValue("@varDID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("DMC");
        }


        public IActionResult DeleteVisa(int? ID)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_TH_VisaAssistance";
            cmdB.Parameters.AddWithValue("@varStepName", "Delete_VisaAssistance");
            cmdB.Parameters.AddWithValue("@varVAID", ID);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            return RedirectToAction("VisaAssistance");
        }

        public IActionResult CloseProject(int? ID)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            try
            {
                conn.Open();
                SqlCommand cmdB = new SqlCommand();
                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Update_Master_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Close_Project");
                cmdB.Parameters.AddWithValue("@varModifiedBy", userId);

                cmdB.Parameters.AddWithValue("@varID", ID);

                cmdB.Connection = conn;

                cmdB.ExecuteNonQuery();
                cmdB.Dispose();
            }
            catch (Exception e)
            {
                Message = "Failed";
                Console.WriteLine(e.ToString());
                return RedirectToAction("ViewProject", new { ID = ID });

            }
            finally
            {

                conn.Close();
            }
            Message = "Success";

            return RedirectToAction("ViewProject", new { ID = ID });
        }




        [HttpPost]
        public IActionResult EditUnitDetails(Unit model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier); // will give the user's userId


            String Name = Request.Form["Name"];
            int ID = int.Parse(Request.Form["UnitID"]);
            if (Name == "")
            {
                ModelState.AddModelError("Error", "Field can not be blanked");
                return View();

            }
            int Exist = 0;

            try
            {
                SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
                conn.Open();
                SqlCommand cmdB = new SqlCommand();

                cmdB.CommandType = CommandType.StoredProcedure;
                cmdB.CommandText = "PROC_PMS_Update_Master_Data";
                cmdB.Parameters.AddWithValue("@varStepName", "Update_Unit");
                cmdB.Parameters.AddWithValue("@varUnit", Name);
                cmdB.Parameters.AddWithValue("@varModifiedBy", userId);
                cmdB.Parameters.AddWithValue("@varID", ID);


                cmdB.Connection = conn;
                SqlDataReader drB = cmdB.ExecuteReader();
                while (drB.Read())
                {
                    Exist = int.Parse(drB["Exist"].ToString());

                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }
            finally
            {


            }
            if (Exist == 1)
            {
                Message = "Failed";
            }
            else
            {
                Message = "Success";

            }
            return RedirectToAction("Unit");
        }


        public IActionResult DeleteCompany(int? id)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_SP_INVOICE";
            cmdB.Parameters.AddWithValue("@varStepName", "DELETE_Company");
            cmdB.Parameters.AddWithValue("@Id", id);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            TempData["Message"] = "";
            TempData["Message"] = "Company deleted succesfully";
            return RedirectToAction("CompanyDetails");
        }


        public IActionResult DeleteInvoice(int? id)
        {

            SqlConnection conn = new SqlConnection(_config.GetConnectionString("ServerString"));
            conn.Open();
            SqlCommand cmdB = new SqlCommand();
            cmdB.CommandType = CommandType.StoredProcedure;
            cmdB.CommandText = "PROC_SP_INVOICE";
            cmdB.Parameters.AddWithValue("@varStepName", "DELETE_Invoice");
            cmdB.Parameters.AddWithValue("@Id", id);

            cmdB.Connection = conn;

            cmdB.ExecuteNonQuery();
            cmdB.Dispose();
            conn.Close();

            TempData["Message"] = "";
            TempData["Message"] = "Invoice deleted succesfully";
            return RedirectToAction("InvoiceDetails");
        }
        #endregion


    

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
